# -*- coding: utf-8 -*-
"""
Created on Mon Oct  5 09:25:13 2020

@author: josep
"""

'''import feedparser

feed = feedparser.parse('https://earlham.edu/news/today/rss/')
print(feed.keys())
print(feed.entries)
print(feed['feed'])
print(feed['bozo'])
print(feed['bozo_exception'])
'''
from bs4 import BeautifulSoup
import urllib.request
import ssl
def get_today_at_earlham():
    ssl._create_default_https_context = ssl._create_unverified_context
    
    opener = urllib.request.urlopen
    url = "https://earlham.edu/news/today/rss/"
    html_doc = opener(url)
    soup = BeautifulSoup(html_doc, 'html.parser')
    items = soup.find_all('item')
    virtual_announcements = []
    local_announcements = []
    for item in items:
        announcement = '\n\t'.join(item.title.contents + item.description.contents)
        announcementl = announcement.lower()
        if 'virtual' in announcementl or 'zoom' in announcementl or 'remote' in announcementl or 'online' in announcementl:
            virtual_announcements.append(announcement)
        else:
            local_announcements.append(announcementl)
    return virtual_announcements, local_announcements