# -*- coding: utf-8 -*-
"""
Created on Wed Oct  7 23:56:10 2020

@author: josep
"""
from AbstractChannel import AbstractChannel, AbstractChanSuite

# Design this first:
# This is a channel specially designed to be connective tissue between
# Clubs and the general audience.
# We have managers of this channel as SOC members
# Their channel title will be called Announcements. There can only be 1 instance of this channel in each server.
# Announcement channels are read only to anyone but the manager bot.
# approvals to post on any of these channels will happen through the SOC Command line 

# Channels that the SOC Channel should have are:
    # Read only to all users:
        # Club announcements
        # Event annoucements
        # Daily mailing (local)
        # Daily mailing (virtual)
        # Server Annoucements
    # Read and post only to conveners of any type:
        # Annoucement Submissions

# Commands that SOC people should be able to do:
    # Get information about a club:
            # how many text channels it has, how many voice channels
            # how many conveners, how many members
            # Who are conveners, who are members
            # Timestamp of most recent text activity in the club.
    # Share information about clubs:
            # post information in their club/event annoucement section
            # approve (or auto approve) posting to their club-buzz channel
            # approve (or auto approve) posting to their campus events channel
            

class SOCChannel(AbstractChannel):
    pass

class SOCChannelSuite(AbstractChanSuite):
    async def process_tasks(clis):
        pass