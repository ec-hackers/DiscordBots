# -*- coding: utf-8 -*-
"""
Created on Sun Nov  8 00:25:28 2020

@author: josep
"""


import discord
from AdminChannel import AdminChanSuite
from EmailSuite import EmailSuite
from random import random
from datetime import datetime
from handles import get_member_by_discord_name


'''
auth_commands
'''
users = []
AUTHENTICATED=20000;

async def make_new_member(member):
    new_member = User(member.guild, member)
    await new_member._init(authChannel=await AdminChanSuite.get_auth_channel_name(member.guild))
    users.append(new_member)
    return new_member
 

async def process_auth(channel, author):
    auth_user = None
    for user in users:
        if user._member.id == author.id:
            auth_user = user
            break
    if auth_user == None:
        print("Failed to find user: "  + author.name + ". Making new one")
        #print(repr(message.channel.guild))
        auth_user = await make_new_member(get_member_by_discord_name(channel.guild, author.name + "#" + author.discriminator))
    if await AdminChanSuite.get_auth_enabled(channel.guild) == False:
        await auth_user.authenticate(auth_user._authcode)
        return AUTHENTICATED
    return auth_user

async def send_auth_code_to_user(channel, auth_user, emailaddress, email_confirm):
    if emailaddress != email_confirm:
        return -1000
    if '@' in emailaddress:
        emailaddress = emailaddress[:emailaddress.index('@')]
    print('send email to ' + emailaddress)
    auth_user = await process_auth(channel, auth_user)
    if auth_user != AUTHENTICATED:
        await auth_user.send_auth_email_to_addr(emailaddress)
        await channel.send('Send auth code to: ' + emailaddress + await AdminChanSuite.get_email_verification_destination_domain(channel.guild), delete_after = 3)

async def authenticate(channel, auth_user, auth_code):
    auth_user = await process_auth(channel, auth_user)
    if auth_user != AUTHENTICATED:
        if await auth_user.authenticate(auth_code.upper()) == AUTHENTICATED:
            users.remove(auth_user)
            await channel.send('Authentication succeeded... Moved you to the full discord. Look at all channels for information!', delete_after = 5)
        else:
            await channel.send('Authentication failed... ' + auth_code + ' is not the correct auth code', delete_after = 3)




'''
General User commands
'''

async def ghelp(channel):
    commands = ['!clublist\n\t\t - Lists all clubs in ' + channel.guild.name, '!describeclub Clubname\n\t\t - Gives a Clubname\'s self-provided description', '!joinclub Clubname\n\t\t - Joins the club Clubname', '!leaveclub\n\t\t - Leaves a server\'s club',
                        '!makeclub Clubname\n\t\t - Makes the club Clubname','!admin\n\t\t - Opens a channel with you and admins', '!report person reason\n\t\t - Sends a report on a person to an admin or club convener.',
                        '!addtextchannel The-name-of-the-public-category The-new-channel\'s-name\n\t\t - Adds a new public text channel in a category', 
                        '!removetextchannel The-name-of-the-public-category The-new-channel\'s-name\n\t\t - Removes the public text channel YOU MADE from the category. Doesn\'t work for other people\'s channels',
                        '!addvoicechannel The-name-of-the-public-category The-new-channel\'s-name\n\t\t - Adds a new public voice channel in a category', 
                        '!removevoicechannel The-name-of-the-public-category The-new-channel\'s-name\n\t\t - Removes the public voice channel YOU MADE from the category. Doesn\'t work for other people\'s channels',]
    send_text = 'Commands in ' + channel.guild.name + ' are:\n\t'
    for command in commands:
        send_text += command + '\n\t'
    await channel.send(send_text)
    return 1


class User:
    def __init__(self, guild, member):
        #send a message to the user asking for their pre-@earlham.edu email part
        # send an auth code
        # if they get the code right, delete them from this class and add them to the ECMember class
        # If after 24 hours they are not an ECMember, kick them out of the server.
        
        self._guild = guild
        self._member = member

        # Get the auth role from the guild.
        
        
        
        
        #Record the time
        #self._time = None
        
        #their authcode:
        self._authcode = 'AUTH' + str(random())
        self._email_prefix = "None00"
        # and their email prefix.
        self._email_prefix = None
    async def _init(self, authRole = 'auth', authChannel = 'please authenticate'):
        '''
        Assert there is an Auth Role in the server. If there is no auth role, immediately kick the user out of the server/
        Assert there is an Auth Channel in the server. If there is no such auth channel, immediately kick the user out of the server/
        
        '''
        # TODO
        
        authRole = await AdminChanSuite.get_auth_channel_name(self._guild)
        authChannel = await AdminChanSuite.get_auth_channel_name(self._guild)
        self._authRole = None
        self._authChannel = None
        for role in self._guild.roles:
            if role.name == authRole:
                self._authRole = role
                break
        for channel in self._guild.text_channels:
            if channel.name == authChannel:
                self._authChannel = channel
                break
        if self._authRole == None:
            self._authRole = await self._guild.create_role(name=authRole, colour=discord.Colour.from_rgb(255,255,255))
            #raise AssertionError("THERE IS NO AUTH ROLE IN THE SERVER... KICKED NEW USER")
        if self._authChannel == None:
            self._authChannel = await self._guild.create_text_channel(authChannel)
            await self._authChannel.set_permissions(self._guild.default_role, read_messages = False)
            await self._authChannel.set_permissions(self._authRole, read_messages=True, send_messages=True)
            await (await self._authChannel.send(await AdminChanSuite.get_auth_chan_message(self._guild))).pin()
            await (await channel.fetch_message(channel.last_message_id)).delete()
            #raise AssertionError("THERE IS NO AUTH CHANNEL IN THE SERVER... KICKED NEW USER")
        
        '''Give the new user authentication priviledges'''
        await self._member.add_roles(self._authRole)
        
        '''AND AUTHENTICATE'''
        # Record authentication start time
        self._now = datetime.now()
        # Send message asking for their pre-earlham@earlham.edu email part (Do i want to do this?)
        
    async def send_auth_email_to_addr(self, email_prefix):
        self._email_prefix = email_prefix
        message = """            
            Hello """ + self._member.name + """,
                Copy \"!auth """+ self._authcode + """\" into the auth discord channel to join the """ + self._guild.name + ' discord server!'
        sender_name = await AdminChanSuite.get_sender_name(self._guild)
        domain_dest = await AdminChanSuite.get_email_verification_destination_domain(self._guild)
        EmailSuite.send_email_to_addr(email_prefix + domain_dest,"Time to join " + self._guild.name + "...", message, sender_name=sender_name)
        
    def getResponse(self):
        # Get all messages. If not number of digits in the authcode, it's an address.
        # Send email out.
        # otherwise, it's an authcode. attempt authorization.
        pass
    async def authenticate(self, authcode):
        # Get their user information. Make an ECMember object and return it with their creds
        '''AUTHENTICATE. FIGURE OUT WHETHER STUDENT OR FACULTY LATER'''
        '''NOT FINISHED. MUST FIX TO BE MORE GENERAL, BUT FOR FUTURE RENDITIONS'''
        if authcode == self._authcode:
            #make an ecmember,
            print('successfully authenticated')
            new_member, role_name = (ECStudent(self._member), await AdminChanSuite.get_authrolename0(self._guild)) if User.valid_student_email(self._email_prefix) else (ECFaculty(self._member), await AdminChanSuite.get_authrolename1(self._guild)) # FIX TO BE MORE GENERAL LATER
            await self._member.remove_roles(self._authRole)
            await new_member._init(role_name=role_name)
            return AUTHENTICATED
        # post message of failed authentication
        # wait three seconds and then delete message of failed authentication
        
        return False #ECMember()
    
    async def attempt_kick(self):
        # kick them out if the time is >= 24 hours after self._now.
        # Deletes them from the server.
        # returns true if kicked. False otherwise.
        timedelta = datetime.now() - self._now
        if timedelta.days >= 1:
            await self._member.kick()
            
    def is_int(string):
        try:
            return int(string)
        except:
            return False
    
    def valid_student_email(earlham_email_handle):
        return True
        year = User.is_int(earlham_email_handle[-2:])
        return (year and 17 <= year <= 20)
class ECMember:
    def __init__(self):
        # give them access to information channel and meet & greet channels.
        #   Give them access to read messages in digital advertisement text channel.
        # Give them ability to dm users, see user roles
        pass
    
class ECStudent(ECMember):
    def __init__(self, member):
        # Give them ability to join and start clubs.
        ECMember.__init__(self)    
        self._member = member
    async def _init(self, role_name='ECSTUDENT', role_color='Magenta'):
        ''' 
        Assert there is an ECSTUDENT ROLE. if there isn't, make one.
        and add general member permissions to said role.
        '''
        student_role_name = role_name
        student_role_color= role_color
        guild = self._member.guild
        student_role = None
        for role in guild.roles:
            if role.name == student_role_name:
                student_role = role
                break
        if student_role == None:
            # make the role and add general permissions
            student_role = await guild.create_role(name=student_role_name, colour=discord.Colour.magenta()) # FIX TO ACTUALLY SUPPORT COLORS LATER
            #await student_role.set_permissions( blah blah)
        await self._member.add_roles(student_role) 
        
        # Give the member the ECSTUDENT role    
        # Give the ECStudent Role
        
        # permissions for botty commands will line up based on the user's object type
        #   Play games

class ECFaculty(ECMember):
    def __init__(self, member):
        # Give them the ability to join and advise clubs.
        ECMember.__init__(self)    
        self._member = member
    
    async def _init(self, role_name='ECFACULTY', role_color='teal'):
        ''' 
        Assert there is an ECFACULTY ROLE. if there isn't, make one.
        and add general member permissions to said role.
        '''
        faculty_role_name = role_name
        faculty_role_color = role_color
        guild = self._member.guild
        faculty_role = None
        for role in guild.roles:
            if role.name == faculty_role_name:
                faculty_role = role
                break
        if faculty_role == None:
            # make the role and add general permissions
            faculty_role = await guild.create_role(name=faculty_role_name, colour=discord.Colour.teal()) # FIX TO ACTUALLY SUPPORT COLORS LATER
            #await faculty_role.set_permissions( blah blah)
        await self._member.add_roles(faculty_role) 
        
clubs = []
users = []