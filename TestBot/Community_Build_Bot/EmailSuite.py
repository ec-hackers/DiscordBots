# -*- coding: utf-8 -*-
"""
Created on Wed Oct 14 16:04:29 2020

@author: josep
"""

import smtplib, ssl
#from AdminChannel import AdminChanSuite
from handles import send_account_email, aws_pass, aws_username
'''
Just a class of email functionalities the bot uses.
'''


class EmailSuite:
    def get_send_email_credentials(guild):
        # Seperate function right now so that we can change how we get credentials later on
        #return (AdminChanSuite.get_send_email_addr(guild), AdminChanSuite.get_send_email_pass(guild))
        return (aws_username,aws_pass)
    
    def verify_email(to_email, login_username=aws_username, login_pass=aws_pass, from_email=send_account_email):
        port = 465
        context = ssl.create_default_context()
        response = None
        with smtplib.SMTP_SSL("email-smtp.us-east-1.amazonaws.com", port, context=context) as server:
            server.login(login_username, login_pass)
            response=server.verify(to_email)
            server.close()
        return response
    def send_email_to_addr(to_email, message_subject, message_text,
                           login_username=aws_username, login_pass = aws_pass, 
                           from_email = send_account_email,
                           sender_name="Community Discord Server"):
        port = 465
        context = ssl.create_default_context()
        response = None
        with smtplib.SMTP_SSL("email-smtp.us-east-1.amazonaws.com", port, context=context) as server:
            server.login(login_username, login_pass)
            # TODO: Send email here
            sender_email = from_email#"@gmail.com"
            receiver_email = to_email
            message = 'From: ' + sender_name + ' <'+ sender_email + '>\r\nTo: '+receiver_email + """\r\nSubject: """ + message_subject + """\r\n\r\n""" + message_text
            
            response = server.sendmail(from_email, receiver_email, message)
            server.close()
        return response