# -*- coding: utf-8 -*-
"""
Created on Sat Sep  5 14:08:39 2020

@author: josep
"""

import discord
from AbstractChannel import AbstractChanSuite, AbstractChannel
from AdminChannel import AdminChanSuite
from datetime import datetime, timedelta
from handles import MIN_BETWEEN_TASK_EXECUTIONS, PROCESSING_TIME_PADDING, get_member_by_discord_name
from EmailSuite import EmailSuite

class Club(AbstractChannel): # Creates a club category channel with CLUB MEMBER role read and write permissions for club members and CLI access for club convenors
    Convenor_cli_permissions = {'read_messages':True,
                                'send_message':True,
                                'connect':True,
                                'speak':True} # WIP Project
    Member_permissions = {'read_messages':True,
                          'send_messages':True,
                          'connect':True,
                          'speak':True}
    default_guild_role_perms = {'read_messages':False,
                                'send_messages':False,
                                'connect':False,
                                'speak': False}
    convenor_color = discord.Colour.blue()
    member_color = discord.Color.greyple()
    channel_type = 'c'
    convener_role_name = 'Convener'
    member_role_name = 'Member'
    
    def __init__(self, guild, convener, clubName):
        # Make a cli where commands from club conveners here can shape club below.
        # Have list of all voice and text channels owned by the club.
        # Can only influence voice/text channels owned by this club object.
        # Clubs are the only things that can act on the server. Conveners can act
        # on club objects.

        #Auto give at least one person convener access to the club.
        # Needs functionality to give other ECStudents convener roles.
        AbstractChannel.__init__(self, guild, convener, clubName, Club.channel_type,
                                 Club.convener_role_name,
                                 Club.member_role_name,
                                 Club.convenor_color,
                                 Club.member_color,
                                 Club.default_guild_role_perms,
                                 Club.Member_permissions,
                                 Club.Convenor_cli_permissions)
        
    async def _init(self):
        await self._guild.create_role(name=self._channelName + " " + Club.member_role_name)
        return await AbstractChannel._init(self, [self._channelName + " " + Club.member_role_name],
                              suite=ClubSuite)
class ClubSuite(AbstractChanSuite):
    def _assert_non_duplicate(guild, channel_name):
        if AbstractChanSuite._assert_non_duplicate(guild, Club.channel_type, channel_name) == -4:
            return -5
        return 1
    
    def get_all_clubs(guild): # returns list of club categories
        return AbstractChanSuite.get_all_chans(guild, Club.channel_type)
    
    '''
    Resolve club category from club category name
    '''
    def get_club(clubs, name):
        return AbstractChanSuite.get_chan(clubs, name)
    
    def get_club_roles(club):
        return AbstractChanSuite.get_chan_roles(club)
    
    '''
    Returns club category that matches proposed cli if there is one.
    '''
    def is_cli(clubs, check_channel):
        return AbstractChanSuite.is_cli(clubs, check_channel, Club.convener_role_name)
    '''
    Returns the cli for the given club category channel
    '''
    
    def get_cli(category):
        return AbstractChanSuite.get_cli(category, Club.convener_role_name)

    
        
        #await club.guild.create_role(club.name + ' ' + role_name, permissions) # Unstable.
    # Add new convener to the club
    async def add_convener(club, member):
        convener_role_name = Club.convener_role_name
        await AbstractChanSuite.add_CLIUser(club, member, convener_role_name)
    
    # Add new members to the club
    async def add_member(club, member):
        member_role_name = club.name + ' ' + Club.member_role_name
        await member.add_roles(ClubSuite.get_role(club, member_role_name))
    
    async def kick_member(club, member, member_role_name=None):
        if member_role_name == None:
            member_role_name = club.name + ' ' + Club.member_role_name
        await member.remove_roles(ClubSuite.get_role(club, member_role_name))
    
    async def remove_as_convenver(club, member):
        await member.remove_roles(ClubSuite.get_role(club, club.name + ' ' + Club.convener_role_name))
    
    async def ban_member(self, member):
        # permaban member from the club
        # do not allow join if on a list, i guess
        pass
    def unban_member(self, member):
        # unban member from the club
        # remove from the list, i guess
        pass
    
    def get_club_commands():
        return ['!describe description - Registers your club description. You can put spaces in description.', 
                '!pin channel-to-pin-to-in-club message-to-pin - Pins message-to-pin to channel-to-pin-to-in-club',
                '!unpin channel-to-unpin-from-in-club copypaste-message-to-unpin - unpins the message copy pasted from pinned messages from channel-to-unpin-from-in-club',
                '!requestbot What you want your bot to do - Ask the Manager Bot Developers to make your club a bot! (They will get back to you with questions)',
                '!kick member - Removes a member',
                '!addvoicechannel name - Adds a voice channel',
                '!addtextchannel name - Adds a text channel',
                '!addconvener member - Makes member a convener', 
                '!ban member - Unimplemented', '!unban member - Unimplemented',
                '!closeclub - Closes your club',
                '!removevoicechannel name - Remove voice channel',
                '!removetextchannel name - Remove text channel',
                '!convenerhelp - Displays this message']
    
    def get_club_command_examples():
        return # not implemented
    
    def get_help_message(channel):
        commands = ClubSuite.get_club_commands()
        send_text = 'Convener Commands in ' + channel.name + ' are:\n\t'
        for command in commands:
            send_text += command + '\n\t'
        return send_text
    
    async def add_private_voice_chan(club, channelName):
        await AbstractChanSuite.add_private_voice_chan(club, channelName, Club.convener_role_name, Club.Convenor_cli_permissions)
    
    async def ui_add_member(club, member_name):
        member = get_member_by_discord_name(club.guild, member_name)
        await ClubSuite.add_member(club, member); return True
    
    async def ui_kick_member(club, member_name):
        member = get_member_by_discord_name(club.guild, member_name)
        await ClubSuite.kick_member(club, member); return True
    
    async def ui_convenerhelp(cli):
        await cli.send(ClubSuite.get_help_message(cli)); return True

    async def ui_describe(cli, description):
        for pin in await cli.pins():
                if pin.content.startswith("DESCRIPTION SET TO:"):
                    await pin.unpin()
        await (await cli.send('DESCRIPTION SET TO: ' + description)).pin()
        return True
    
    async def ui_pin(channel, TextChannel, content):
        channel = await ClubSuite.get_text_channel(channel.guild.get_channel(channel.category_id), TextChannel)
        await (await channel.send(content)).pin()
        return 1
    async def ui_unpin(channel, TextChannel, content):
        channel = await ClubSuite.get_text_channel(channel.guild, TextChannel)
        for pin in await channel.pins():
            if pin.content == content:
                await pin.unpin()
                return True
        return -217
    
    async def ui_schedule_message(cli, date, time, content, channel):
        await (await cli.send('MESSAGE SCHEDULED for ' + date + " @ " + time + ' "' + content + '" to ' + channel)).pin()
        return 1
    
    async def ui_unschedule(cli, content):
        for pin in await cli.pins():
            if pin.content == content:
                await pin.delete()
                return 1
        return -11
    
    async def ui_weekly_reminder(cli,weekday, time, content, channels):
        await (await cli.send('Now meeting weekly on '+ weekday + 's at ' + time + ' . Posting ' + content + ' in ' + str(channels))).pin()
        return 1
    
    async def ui_cancel_weekly_reminder(cli, content):
        for message in cli.pins():
            if message.content == content:
                message.delete()
                return 1
        return -12
    
    async def ui_setup_email(cli, channel, discord_name, dest_email, confirm_dest_email):
        if dest_email != confirm_dest_email:
            return -1
        await (await cli.send('EMAIL LINK ESTABLISHED IN ' + channel + ' . ' +discord_name + ':' + dest_email)).pin()
        return 1
    
    async def ui_requestbot():
        return 6
    
    async def ui_describeclub(channel, target_club_name):
        club = ClubSuite.get_club(ClubSuite.get_all_clubs(channel.guild), target_club_name)
        cli = ClubSuite.get_cli(club)
        for pin in await cli.pins():
            if pin.content.startswith('DESCRIPTION SET TO: '):
                await channel.send(target_club_name + '\'s description is: ' + pin.content[len('DESCRIPTION SET TO: '):])
                return 1
        return -9
    
    async def get_club_description(cli):
        for pin in await cli.pins():
            if pin.content.startswith('DESCRIPTION SET TO: '):
                return pin.content[len('DESCRIPTION SET TO: '):]
        return "No Description Posted!"
    
    async def ui_clublist(channel):
        clubs = ClubSuite.get_all_clubs(channel.guild)
        send_text = 'The clubs in ' + channel.guild.name + ' are: \n\t'
        for club in clubs:
            send_text += club.name[2:] + '\n\t'
        await channel.send(send_text)
        return 1
    
    async def ui_clublist_w_descriptions(channel):
        clubs = ClubSuite.get_all_clubs(channel.guild)
        send_text = []
        for club in clubs:
            send_text.append(club.name[2:] + '\n\t' + '-' + await ClubSuite.get_club_description(ClubSuite.get_cli(club)) + '\n')
        return send_text
    
    async def makeClub(guild, convener, name):
        newClub = Club(guild, convener, name)
        return await newClub._init()
    
    async def process_cli_commands(message):
        content = message.content.lower()
        message_parts = content.split(' ')
        
        message_parts_ = message.content.split(' ')
        club = AbstractChanSuite.is_cli(AbstractChanSuite.get_all_chans(message.guild, Club.channel_type), message.channel, Club.convener_role_name)
        if not club:
            return False # Did not process commands
        
        if content.startswith('!addtextchannel '): # Add club text channel
            await ClubSuite.add_text_channel(club, content[len('!addtextchannel '):]); return True
        elif content.startswith('!removetextchannel'): # remove club text channel
            await ClubSuite.remove_text_channel(club, content[len('!removetextchannel '):].replace(' ', '-')); return True
        elif content.startswith('!addvoicechannel'): #add club voice channel
            await ClubSuite.add_voice_channel(club, content[len('!addvoicechannel '):]);
            return 7
        elif content.startswith('!removevoicechannel'): # remove club voice channel
            return await ClubSuite.remove_voice_channel(club, content[len('!removevoicechannel '):].replace(' ', '-')); return True
            
        elif content.startswith('#!newpvoicechannel'):
            return await ClubSuite.add_private_voice_chan(club, content[len('!newpvoicechannel '):])
            
        elif content.startswith('#!newptextchannel'):
            return await ClubSuite.add_private_text_chan(club, content[len('!newptextchannel '):])
        elif content.startswith('!closeclub'):
            return await ClubSuite.delete_category(club); return 4
        elif content.startswith('!addmember'):
            return await ClubSuite.ui_add_member(club, message.content[len('!addmember '):])
        elif content.startswith('!addconvener'):
            member = get_member_by_discord_name(message.guild, message.content[len('!addconvener '):])#'''ADDCONVENER NEEDS SOME WORK'''
            return await ClubSuite.add_convener(club, member); return True
        elif content.startswith('!kick'):
            return await ClubSuite.ui_add_member(club, content[len('!kick '):])
            
        elif content.startswith('!convenerhelp'):
            return await ClubSuite.ui_convenerhelp(message.channel)
        elif content.startswith('!describe '):
            return await ClubSuite.ui_describeclub(message.channel, message.content[len('!describe '):])
        elif content.startswith('!pin'):
            terms = message.content.split(' ')
            terms[1] = message_parts[1]
            if len(terms) < 3:
                await message.channel.send('!pin requires 3 things! Here\'s an example: !pin MyClubTextChannel what I want to pin and you can include spaces here', delete_after = 10)
            channel = await ClubSuite.get_text_channel(club, terms[1])
            if channel != False:
                return await ClubSuite.ui_pin(channel, ' '.join(terms[2:]))
        elif content.startswith('!unpin'):
            terms = message.content.split(' ')
            terms[1] = message_parts[1]
            if len(terms) < 3:
                await message.channel.send('!pin requires 3 things! Here\'s an example: !pin MyClubTextChannel what I want to pin and you can include spaces here', delete_after = 10)
            channel = await ClubSuite.get_text_channel(club, terms[1])
            content = ' '.join(terms[2:])
            if channel != False:
                return await ClubSuite.ui_unpin(channel, content)
        elif content.startswith('!removemessage'):
            terms = message.content.split(' ')
            terms[1] = message_parts[1]
            if len(terms) < 3:
                return -13
            channel = await ClubSuite.get_text_channel(club, terms[1])
            content = ' '.join(terms[2:])
            if await ClubSuite.remove_text_msg_from_chan(channel, content):
                return True
            await message.channel.send('Couldn\'t find "' + content + '" in ' + terms[1], delete_after = 10)
        elif content.startswith('!schedule message'): #!schedule message date time channel message content
            terms = message.content.split(' ')
            return await ClubSuite.ui_schedule_message(message.channel, terms[2], terms[3], ' '.join(message_parts[5:]), terms[4])
        elif content.startswith('!unschedule '):
            content =message.content[len('!unschedule '):]
            
        elif content.startswith('!weeklymeetingtime'): # !weeklymeetingtime date time [channels to alert] message_content
            terms = message.content.split(' ')
            channels_to_alert = []

            '''
            Make sure we are working with a valid weekday.
            '''
            weekday = terms[1]
            weekdayl = weekday.lower()
            if weekdayl == 'monday':
                weekday = 'Monday'
            elif weekdayl == 'tuesday':
                weekday = "Tuesday"
            elif weekdayl == 'wednesday':
                weekday = 'Wednesday'
            elif weekdayl == 'thursday':
                weekday = 'Thursday'
            elif weekdayl == 'friday':
                weekday = 'Friday'
            elif weekdayl == 'saturday':
                weekday = 'Saturday'
            elif weekdayl == 'sunday':
                weekday = 'Sunday'
            else:
                return -17
            
            '''
            Verify this is a valid time.
            '''
            try:
                hr, mi = terms[2].split(':')
                if not (0 <= int(hr) <= 23 and 0 <= int(mi) < 60):
                    return -19
            except:
                return -18
            
            '''
            Verify all the text channels in the text channel list are valid within the context of the structure of the club.
            '''
            if '[' in terms[3]:
                chan = AbstractChanSuite.is_text_channel(club, terms[3][1:])
                if chan != -15:
                    check_chan = channels_to_alert.append(chan.name)
                else:
                    return -15
                for i in range(4, len(terms)):
                    if ']' in terms[i]:
                        chan = AbstractChanSuite.is_text_channel(club, terms[i][:-1])
                        if chan != -15:
                            check_chan = channels_to_alert.append(chan.name)
                        else:
                            return -15
                        break
                    chan = AbstractChanSuite.is_text_channel(club, terms[i])
                    if chan != -15:
                        check_chan = channels_to_alert.append(chan.name)
                    else:
                        return -15
                    if i == len(terms) - 1:
                        return -16
            else:
                i = 3
                chan = AbstractChanSuite.is_text_channel(club, terms[3])
                if chan != -15:
                    check_chan = channels_to_alert.append(chan.name)
                else:
                    for chan in club.text_channels:
                        channels_to_alert.append(chan.name)
            '''
            Find the message content.
            '''
            
            if i == len(terms) - 1:
                content = ''
            else:
                content = ' '.join(terms[i+1:])
            return await ClubSuite.ui_weekly_reminder(message.channel, weekday, terms[2], ('"Weekly meeting in less that 15! Come soon!"' if content == '' else '"' + content + '"'), channels_to_alert)
        elif content.startswith('!cancelmeetingtime'):
            content = content[len('!cancelmeetingtime '):]
            return await ClubSuite.ui_cancel_weekly_reminder(message.channel, content)
        elif content.startswith("!setupemail"): #!setupemail discordname#id:email channel_to_be_active_in
            colon_containing = 0
            for colon_containing in range(len(message_parts)):
                if ':' in message_parts_[colon_containing]:
                    break
            
            discord_name, dest_email = message_parts_[colon_containing].split(':')
            if colon_containing != 1:
                discord_name = ' '.join(message_parts_[1:colon_containing] + [discord_name])
            # Confirm that the discord user in the server at the moment.
            user = get_member_by_discord_name(message.guild, discord_name)
            if not user:
                return -21
            # now we know the user is in there. Verify email is valid.
            # pass
            # and now we know the email is valid. get the specified channel.
            
            chan = AbstractChanSuite.is_text_channel(club, message_parts[-1])
            if chan != -15:
                pass
            else:
                return -15
            return await ClubSuite.ui_setup_email(message.channel, chan, discord_name, dest_email)
            
            
        elif content.startswith('!requestbot'):
            return ClubSuite.ui_requestbot()
        elif content.startswith('!commandpanel'):
            await ClubControlPanel._init_(message.channel)
            return 1
        return 0
    
    async def process_user_commands(message):
        # for case sensitive commands.
        raw_text = message.content
        raw_text_parts = raw_text.split(' ')
        
        # for non case sensitive commands
        lower_text = message.content.lower()
        lower_text_parts = lower_text.split(' ')
        
        '''
        non case sensitive commands go first
        '''
        if lower_text.startswith('!describeclub') or lower_text.startswith('!describe'):
            target_club_name = '-'.join(lower_text_parts[1:])
            cli = ClubSuite.get_cli(ClubSuite.get_club(ClubSuite.get_all_clubs(message.channel.guild), Club.channel_type + '-' + target_club_name))
            if cli != False:
                return ClubSuite.ui_describeclub(cli, target_club_name)
            return -10
        elif lower_text_parts[0] == '!joinclub':
            target_club_name = '-'.join(lower_text_parts[1:])
            target_club_name = Club.channel_type + '-' + target_club_name
            target_club = ClubSuite.get_club(ClubSuite.get_all_clubs(message.channel.guild), target_club_name)
            if target_club:
                await ClubSuite.add_member(target_club, message.author)
                return 1
            return -10
        elif lower_text_parts[0] == '!leaveclub':
            target_club_name = '-'.join(lower_text_parts[1:])
            target_club_name = Club.channel_type + '-' + target_club_name
            target_club = ClubSuite.get_club(ClubSuite.get_all_clubs(message.channel.guild), target_club_name)
            if target_club:
                await ClubSuite.kick_member(target_club, message.author)
                return 1
            return -10
            # Make club's initializer should get rid of unacceptable characters and return a succeeded or failed error code depending on outcome.
        elif lower_text_parts[0] == '!clublist':
            return ClubSuite.ui_clublist(message.channel)
        elif lower_text_parts[0] == '!makeclub':
            clubName = '-'.join(lower_text_parts[1:]).upper()
            special_chars = "()[]'" + '"' + '!@#$%^&*=+/?\;:`~<>|'
            for char in special_chars:
                if char in clubName:
                    return (-8, char, special_chars)                
            # ClubChannel should auto check for duplicates, so dont worry about dupe checking
            return ClubSuite.makeClub(message.guild, message.author, clubName)
        
        return False
    
    async def process_user_at_messages(message):
        # Verify that we're in a club right now 
        if not message.content.startswith('<@!'):
            return 0
        
        cat = message.channel.category_id
        if cat != None:
            cat = message.guild.get_channel(cat)
        if not cat.name.startswith(Club.channel_type + '-'):
            return 0
        
        # And check to see if the mentioned person is linked to an email in the cli.
        
            # Get the linked messages from the cli
        cli = ClubSuite.get_cli(cat)
        message_parts = message.content.split(' ')
        uid = message_parts[0][3:-1]
        target_chan = None
        discord_user = None
        dest_email_addr = None
        
        
        for pin in await cli.pins():
            if pin.content.startswith('EMAIL LINK ESTABLISHED IN '):
                pin_parts = pin.content.split(' ')
                target_chan = await ClubSuite.get_text_channel(cat, pin_parts[4])
                
                discord_name = ''
                if len(pin_parts) == 7:
                    discord_name, dest_email_addr = pin_parts[6].split(':')
                else:
                    for ind, part in enumerate(pin_parts[6:]):
                        if ':' in part:
                            last_part_of_disc_name, dest_email_addr = part.split(':')
                            discord_name += ' ' + last_part_of_disc_name
                            break
                        discord_name += ' ' + part
                    discord_name = discord_name[1:]
                print(discord_name)
                discord_user = get_member_by_discord_name(message.guild, discord_name)
            # If we mentioned the user in question, email them!
                if int(discord_user.id) == int(uid) and message.channel == target_chan:
                    sender_name = await AdminChanSuite.get_sender_name(message.guild)
                    await cli.send('Sent email alert to: '+ dest_email_addr +  '.', delete_after=3)
                    EmailSuite.send_email_to_addr(dest_email_addr, 'You have been mentioned in ' + message.channel.name + '!', 'Here is the message ' + message.author.name + ' mentioned you in! :\n\t' + discord_name + ' ' + ' '.join(message_parts[1:]), sender_name=sender_name)
                    return 
        else:
            return 0
        
        
    async def process_cli_at_messages(message):
        return 0
    
    async def process_commands(message):
        return await ClubSuite.process_cli_commands(message) or await ClubSuite.process_user_commands(message)
    
    async def process_at_messages(message):
        return await ClubSuite.process_cli_at_messages(message) or await ClubSuite.process_user_at_messages(message)
    
    async def process_tasks(clis):
        print('processsing tasks')
        for cli in clis:
            if not cli.name.startswith(Club.channel_type + '-'): # Then it's not a club cli, and should be treated accordingly.
                continue
            print(cli.name)
            for pin in await cli.pins():
                if pin.content.startswith('Now meeting weekly on'):
                    print('invoked weekly meeting processing')
                    message_parts = pin.content.split(' ')
                    now = datetime.now()
                    if message_parts[4] == now.strftime('%A') + 's':
                    #if 1 == 1:
                        hour, minute = message_parts[6].split(':')
                        #print('sec_diff is: ', (datetime(now.year, now.month,now.day, int(hour), int(minute)) - now + timedelta(seconds=PROCESSING_TIME_PADDING)).seconds, 'delta_int is:', MIN_BETWEEN_TASK_EXECUTIONS*60)
                        #print('timedelta is: ',(datetime(now.year, now.month,now.day, int(hour), int(minute)) - now + timedelta(seconds=PROCESSING_TIME_PADDING)).seconds)
                        if (datetime(now.year, now.month,now.day, int(hour), int(minute)) - now + timedelta(seconds=PROCESSING_TIME_PADDING)).seconds< MIN_BETWEEN_TASK_EXECUTIONS*60:
                            ''' extract content'''
                            content_start = 0
                            content_end = 0
                            pincontent = pin.content
                            for i in range(len(pincontent)):
                                if pincontent[i] == '"':
                                    if not content_start:
                                        content_start = i + 1
                                    else:
                                        content_end = i
                                        break
                                    
                            content = pin.content[content_start:content_end] # please fix later for more speed lol
                            for textchannel in cli.category.text_channels:
                                if textchannel.name in pin.content:
                                    await textchannel.send(content) # Also do one for day before, 
                elif pin.content.startswith('MESSAGE SCHEDULED for '):
                    print('invoked message scheduling')
                    message_parts = pin.content.split(' ')
                    now = datetime.now()
                    month,day,year = message_parts[3].split('/')
                    #print(month,day,year)
                    #print('now:', now.month, now.day, now.year)
                    if now.day == int(day) and now.month == int(month) and now.year == int(year):
                        hour, minute = message_parts[5].split(':')
                        #print(hour, minute)
                        sec_diff = (datetime(now.year, now.month,now.day, int(hour), int(minute)) - now + timedelta(seconds= PROCESSING_TIME_PADDING)).seconds
                        #print('minutes difference is: ', min_diff)
                        if -1*MIN_BETWEEN_TASK_EXECUTIONS//3 *60 <= sec_diff < MIN_BETWEEN_TASK_EXECUTIONS * 60:
                            for textchannel in cli.guild.get_channel(cli.category_id).text_channels:
                                if textchannel.name == message_parts[-1]:
                                    await textchannel.send(' '.join(message_parts[6:-2])) # Also do one for day before
                            await pin.delete()
                        else:
                            print('rejected within 15 min')
                            
from UI import ClubControlPanel
