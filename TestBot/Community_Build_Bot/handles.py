# -*- coding: utf-8 -*-
"""
Created on Sat Sep  5 14:05:37 2020

@author: josep
"""
import discord
import smtplib, ssl
from datetime import datetime
from random import random
import json

DEV_MODE = False

with open('../../../ECHackersBotCreds/credentials' + ('_test_bot.json' if DEV_MODE else '.json')) as file:
    credentials = json.load(file)
key = open("../../../ECHackersBotCreds/secret.key", "rb").read()

server_vars = {'sender_name=':'Discord++',
               'auth_role_name0=':'',
               'auth_role_name1=':'',
               'auth_channel_name=':'auth',
               'auth_channel_enabled=':'True',
               'email_verification_destination_domain=':'',
               'auth_channel_welcome_text=':'''Welcome! To get started, please type in an '!' followed by your school email \n
Ex. !jnislam18@earlham.edu\n
You should see an email from Discord++ in your zimbra email. Enter the code into the chat.''',
'version=': '0.1.5.6'}

auth_channel = 'auth'
domain_dest = '@earlham.edu'
send_account_email='earlhamhackerscontrol@gmail.com'
bot_token = credentials['bot_token']
aws_username = credentials['aws_username']
aws_pass = credentials['aws_pass']


student_role_name = 'ECStudent'
faculty_role_name = 'ECFaculty'
general_user_role_names = [student_role_name, faculty_role_name]


MIN_BETWEEN_TASK_EXECUTIONS = 15
PROCESSING_TIME_PADDING = 30

def get_member_by_discord_name(guild, discord_name):
    for member in guild.members:
        print(member.name + "#" + member.discriminator, discord_name)
        if member.name + "#" + member.discriminator == discord_name:
            return member
    return False
