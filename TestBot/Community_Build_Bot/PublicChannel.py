# -*- coding: utf-8 -*-
"""
Created on Sat Sep  5 14:06:37 2020

@author: josep
"""

import discord

from AbstractChannel import AbstractChanSuite, AbstractChannel
# Public Channels are areas where people can make text and voice channels
# Whenever they want to

class PublicChan: # Creates a club category channel with CLUB MEMBER role read and write permissions for club members and CLI access for club convenors
    Convenor_cli_permissions = {'read_messages':True,
                                'send_message':True}
    Member_permissions = {'read_messages':True,
                          'send_messages':True,
                          'connect':True,
                          'speak':True}
    default_guild_role_perms = {'read_messages':True,
                                'send_messages':True,
                                'connect':True,
                                'speak': True}
    convenor_color = discord.Colour.red()
    member_color = discord.Color.greyple()
    Member_abstract_permissions = None
    channel_type = 'o'
    convener_role_name = 'Moderator'
    member_role_name = 'none'
    convenor_color = discord.Colour.red()
    def __init__(self, guild, moderator, channelName):
        # Make a cli where commands from club conveners here can shape club below.
        # Have list of all voice and text channels owned by the club.
        # Can only influence voice/text channels owned by this club object.
        # Clubs are the only things that can act on the server. Conveners can act
        # on club objects.

        #Auto give at least one person convener access to the club.
        # Needs functionality to give other ECStudents convener roles.
        AbstractChannel.__init__(self, guild, moderator, channelName, PublicChan.channel_type,
                                 PublicChan.convener_role_name,
                                 PublicChan.member_role_name,
                                 PublicChan.convenor_color,
                                 PublicChan.member_color,
                                 PublicChan.default_guild_role_perms,
                                 PublicChan.Member_permissions,
                                 PublicChan.Convenor_cli_permissions)
        
    async def _init(self, general_user_roles):
        '''
        CREATE & ASSIGN ROLES. (Expandable to include mods, operators, etc.)
        '''
        return await AbstractChannel._init(self, general_user_roles,
                              suite=PubChanSuite)
        
        
class PubChanSuite(AbstractChanSuite):
    def _assert_non_duplicate(guild, channel_name):
        if AbstractChanSuite._assert_non_duplicate(guild, PublicChan.channel_type, channel_name) == -4:
            return -7
        return 1
    def get_all_pubchans(guild): # returns list of club categories
        return AbstractChanSuite.get_all_chans(guild, PublicChan.channel_type)
    
    '''
    Resolve club category from club category name
    '''
    def get_pubchan(chans, name):
        return AbstractChanSuite.get_chan(chans, name)
    
    def get_pubchan_roles(club):
        return AbstractChanSuite.get_chan_roles(club)
    
    '''
    Returns club category that matches proposed cli if there is one.
    '''
    def is_cli(categories, channel):
        return AbstractChanSuite.is_cli(categories, channel, PublicChan.convener_role_name)

    def get_cli(category):
        return AbstractChanSuite.get_cli(category, PublicChan.convener_role_name)
    
    # Add new convener to the club
    async def add_moderator(club, member):
        await AbstractChanSuite.add_CLIUser(club, member, PublicChan.convener_role_name)

    async def remove_as_moderator(chan, member):
        await member.remove_roles(PubChanSuite.get_role(chan, chan.name + ' ' + PublicChan.convener_role_name))
        
    async def ban_member(self, member):
        # permaban member from the pubchannel
        # do not allow join if on a list, i guess
        pass
    def unban_member(self, member):
        # unban member from the pubchannel
        # remove from the list, i guess
        pass
    
    
    def get_pubchan_commands():
        return ['!addrole - notimplemented','!addmember [member]- adds a member','!kick [member] - removes a member','!addvoicechannel [name] - adds a voice channel','!addtextchannel [name] - adds a text channel','!addconvener [member] - makes [member] a convener', '!ban [member] - unimplemented', '!unban [member] - unimplemented', '!closeclub - closes your club',
                '!removevoicechannel [name] - remove voice channel', '!removetextchannel [name] - remove text channel']
    def get_pubchan_command_examples():
        return # not implemented
    
    def get_help_message(channel):
        commands = PubChanSuite.get_pubchan_commands()
        send_text = 'Moderator Commands in ' + channel.name + ' are:\n\t'
        for command in commands:
            send_text += command + '\n\t'
        return send_text    
    
    async def process_cli_commands(message):
        content = message.content.lower()
        message_parts = content.split(' ')
        o_chan = AbstractChanSuite.is_cli(AbstractChanSuite.get_all_chans(message.guild, PublicChan.channel_type), message.channel, PublicChan.convener_role_name)
        if not o_chan:
            return False # Did not process commands
        
        if content.startswith('!removemessage'):
            terms = message.content.split(' ')
            terms[1] = message_parts[1]
            if len(terms) < 3:
                await message.channel.send('!removemessage requires 3 things! Here\'s an example: !removemessage MyClubTextChannel text from message i want to remove', delete_after = 10)
            channel = await PubChanSuite.get_text_channel(o_chan, terms[1])
            content = ' '.join(terms[2:])
            if await PubChanSuite.remove_text_msg_from_chan(channel, content):
                return True
            await message.channel.send('Couldn\'t find "' + content + '" in ' + terms[1], delete_after = 10)
            
    
    async def process_user_commands(message):
        content = message.content.lower()
        message_parts = content.split(' ')
        if len(message_parts) >= 3:
            pubchan = PubChanSuite.get_pubchan(PubChanSuite.get_all_pubchans(message.channel.guild), PublicChan.channel_type + '-' + message_parts[1])
            if pubchan:
                target = pubchan.name
                channel = '-'.join(message_parts[2:])
                if content.startswith('!addtextchannel'):
                    start_role = await message.channel.guild.create_role(name=target + ' ' + channel + ' Starter') # Someone has to streamline this. THis is shit.
                    await message.author.add_roles(start_role)
                    await PubChanSuite.add_text_channel(pubchan, channel)
                    return True
                elif content.startswith('!removetextchannel'):
                    if target + ' ' + channel + ' Starter' in [role.name for role in message.author.roles]:
                        for role in message.channel.guild.roles:
                            if role.name == target + channel + ' Starter':
                                await message.author.remove_roles(role)
                                await role.delete()
                                break
                        await PubChanSuite.remove_text_channel(pubchan, channel)
                        return True
                elif content.startswith('!addvoicechannel'):
                    start_role = await message.channel.guild.create_role(name=target + ' ' + channel + ' Starter') # Someone has to streamline this. THis is shit.
                    await message.author.add_roles(start_role)
                    await PubChanSuite.add_voice_channel(pubchan, channel)
                    return True
                elif content.startswith('!removevoicechannel'):
                    if target + ' ' + channel + ' Starter' in [role.name for role in message.author.roles]:
                        pubchan = PubChanSuite.get_pubchan(PubChanSuite.get_all_pubchans(message.channel.guild), target)
                        for role in message.channel.guild.roles:
                            if role.name == target + channel + ' Starter':
                                await message.author.remove_roles(role)
                                await role.delete()
                                break
                        await PubChanSuite.remove_voice_channel(pubchan, channel)
                        return True
                return False
    async def process_commands(message):
        return await PubChanSuite.process_cli_commands(message) or await PubChanSuite.process_user_commands(message)
    
    async def process_tasks(clis):
        pass