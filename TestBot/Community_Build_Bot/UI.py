# -*- coding: utf-8 -*-
"""
Created on Fri Nov  6 15:56:37 2020

@author: josep
"""
from ClubChannel import Club, ClubSuite
from PublicChannel import PublicChan, PubChanSuite
from AdminChannel import AdminChannel, AdminChanSuite
from restrictedchannel import RestrictChan, RestrictChanSuite
from datetime import datetime
from handles import get_member_by_discord_name

from enums import *

class ControlPanel: # little bit of confusion up here. Will map out tomorrow
    #BUTTONS = [] # List of strings detailing what buttons mean.
    BUTTONS = {} # {Func enum: 'quick description of the button's function'}
    STARTING_TEXT = "Click a button to do something!"
    MAX_AT_A_TIME = 6
    async def _init_(channel, buttons, menu_type):
        await UI.prompt_option_from_option_list_from_user(channel, menu_type + ") " + ControlPanel.STARTING_TEXT, buttons.values(), ControlPanel.MAX_AT_A_TIME)
        
    async def click(client, channel, selected_option, buttons):
        func_enum = None
        for enum, option in buttons.items():
            print(option, selected_option)
            if option == selected_option:
                func_enum = enum
        print(func_enum)
        if func_enum == None:
            return 0
        tagstring = TAGSTRINGS[func_enum]
        await UI.prompt(client, func_enum, channel)
        
    async def process_finished_prompt(func_enum, channel, reactor, returned_string ):
        tags = ControlPanel.resolve_var_from_tag_returns(TAGSTRINGS[func_enum], returned_string)
        return await WRAPPERMAP[func_enum](**tags, channel=channel, user=reactor)
        
    def resolve_var_from_tag_returns(tagstring, return_string):
        tags = []
        for item in tagstring.split(' '):
            if item in TAGS.values():
                tags.append(item[1:-1])
        
        assigned_tags = {}
        i = 0
        j = 0
        while i < len(return_string):
            if return_string[i] == '[':
                j = i+1
                while j < len(return_string):
                    if return_string[j] == ']':
                        assigned_tags[tags.pop(0)] = return_string[i + 1:j]
                        break
                    j += 1
                i = j
            i += 1
        return assigned_tags

class ClubControlPanel(ControlPanel):
    BUTTONS = {CADDTEXTCHANNEL:"Add a text channel",
               CREMOVETEXTCHANNEL: "Remove a text channel",
               CADDVOICECHANNEL: "Add a voice channel",
               CREMOVEVOICECHANNEL:"Remove a voice channel",
               CDESCRIBE:"Set your club description",
               CADDCONVENER: "Add a co-convener",
               CKICK: "Kick a club member from your club",
               CPIN: "Pin a message to a channel in your club",
               CSCHEDULEMESSAGE: "Schedule a message to be sent in a club channel",
               CWEEKLYREMINDER: "Schedule a weekly reminder to be sent in a club channel",
               CSETUPEMAIL: "Make it so if soemone @ messages you, discord++ will send you an email",
               
               CUNPIN: "Unpin a message from one of your club's channels",
               CUNSCHEDULEMESSAGE:"Unschedule a weekly reminder",
               CUNSCHEDULEWEEKLYREMINDER:"UNschedule a weekly reminder",
               CCLOSECLUB:"Delete your club"} # {Func enum: 'quick description of the button's function'}
    async def _init_(channel):
        await ControlPanel._init_(channel, ClubControlPanel.BUTTONS, "C")
        return CCONTROLPANELADDITIONSUCCESS
    async def click(client, channel, selected_option):
        await ControlPanel.click(client, channel, selected_option, ClubControlPanel.BUTTONS)
        return CCONTROLPANELCLICKED
class AuthControlPanel(ControlPanel):
    BUTTONS = {USENDAUTHEMAIL: "Get an email sent to your school account!",
               #UAUTHENTICATE: "Fully enter your school discord"
        }
    SIGNATURE = 'u'
    async def _init_(channel):
        await ControlPanel._init_(channel, AuthControlPanel.BUTTONS, AuthControlPanel.SIGNATURE)
        return UCONTROLPANELADDITIONSUCCESS
    async def click(client, channel, selected_option):
        await ControlPanel.click(client, channel, selected_option, AuthControlPanel.BUTTONS)
        return UCONTROLPANELCLICKED

class GeneralControlPanel(ControlPanel):
    BUTTONS = {GCLUBLIST: "Find out what clubs exist in the server",
            GJOINCLUB: "Join a club in the server",
               GLEAVECLUB: "Leave a club in the server",
               
               GDESCRIBECLUB: "Describe a club in the server",
               GMAKECLUB: "Make a club"
               } # {Func enum: 'quick description of the button's function'}
    SIGNATURE = 'g'
    async def _init_(channel):
        await ControlPanel._init_(channel, GeneralControlPanel.BUTTONS, GeneralControlPanel.SIGNATURE)
        return CCONTROLPANELADDITIONSUCCESS
    async def click(client, channel, selected_option):
        print('clicked general control panel')
        if selected_option == GeneralControlPanel.BUTTONS[GCLUBLIST]:
            return await WRAPPERMAP[GCLUBLIST](channel=channel, user='None')
        print('not clublist')
        await ControlPanel.click(client, channel, selected_option, GeneralControlPanel.BUTTONS)
        print('hello')
        return CCONTROLPANELCLICKED
    
class AdminControlPanel(ControlPanel):
    BUTTONS = {AADDPUBCHAN: 'Add an open channel',
               AADDRESTRICTCHAN: 'Add a restricted channel',
               ATOGGLEAUTH: "Toggle the auth channel",
               ASETDESTDOMAINAS: "Change the email authentication goes to",
               ASETSENDERNAMEAS: "Set the sender name of the authentication email",
               ASETWELCOMEMESSAGEAS: "Set the welcome message in the auth channel",
               ASETAUTHCHANNAMEAS: 'Change the auth channel name',
               ASETAUTHROLE0AS: "Change the default role name",
               ASETAUTHROLE1AS: "Change the special role name"
               
               
        }
    SIGNATURE='A'
    async def _init_(channel):
        await ControlPanel._init_(channel, AdminControlPanel.BUTTONS, AdminControlPanel.SIGNATURE)
        return UCONTROLPANELADDITIONSUCCESS
    async def click(client, channel, selected_option):
        await ControlPanel.click(client, channel, selected_option, AdminControlPanel.BUTTONS)
        return UCONTROLPANELCLICKED

class TextLister(ControlPanel):    
    BUTTONS = {GCLUBLIST:{GJOINCLUB: "joining a club",
                          GLEAVECLUB: " leave a club"}} # {TEXTGENERATOR:{WRAPPERMAP: 'Prompt for each option'}}
    MAX_AT_A_TIMES = {GCLUBLIST:4} # {TEXTGENERATOR:max_entries} mapping
    SIGNATURE = 'T'
    async def _init_(channel, func_enum):
        return await UI.text_lister(channel, func_enum, TextLister.SIGNATURE)
        
    async def click(client, channel, func_id, selected_option):
        await ControlPanel.click(client, channel, selected_option, TextLister.BUTTONS[func_id])
        return CCONTROLPANELCLICKED
CONTROLMAP[GCLUBLIST] = TextLister._init_

class UI:
    NEXT_BUTTON = "\U000027a1"
    PREV_BUTTON = "\U00002b05"
    CANCEL_BUTTON = "\U0001F6AB"
    CLOSE_BUTTON = "\U000026D4"
    
    SUCCESSFULLY_ROTATED_OPTION = 11000
    INVALID_CHOSEN_OPTION = 11001
    
    async def prompt_text(func_enum, channel):
        options = ['finish']
        await UI.prompt_option_from_option_list_from_user(channel, TAGPROMPTS.get(func_enum, TAGPROMPTS[GENERAL])[TAGS[TEXT]], options, 10)
    
    async def process_text(client, message, user):
        assert client.user == message.author, "This isn't an inquiry sent by our bot. Do better code."
        proceed = False
        for prompts in TAGPROMPTS.values():
            if message.content.startswith(prompts.get(TAGS[TEXT], TAGPROMPTS[GENERAL][TAGS[TEXT]])):
                proceed = True
                break
        if not proceed:
            return 0
        
        selected = await UI.preprocess_reaction_selection_menu(client, message, user, 1)
        if selected:
            for ind, emoji in GENERAL_OPTION_EMOJIS.items():
                if selected[0].emoji == emoji:
                    selected[0] = 'done'
                    msg = await message.channel.fetch_message(message.channel.last_message_id)
                    if msg != message:
                        prompter_response = await UI.prompter(client, message.channel, TAGS[TEXT], msg.content, reactor=user)
                        if prompter_response != PROMPTERDIDNOTHING:
                            await message.delete()
                            await msg.delete()
                            return prompter_response
                    await message.clear_reactions()
                    return 1
        return 0
    
    async def prompt_email(func_enum, channel):
        options = ['finish']
        await UI.prompt_option_from_option_list_from_user(channel, TAGPROMPTS.get(func_enum, TAGPROMPTS[GENERAL])[TAGS[EMAILADDRESS]], options, 10)
        
    async def process_email(client, message, user):
        assert client.user == message.author, "This isn't an inquiry sent by our bot. Do better code."
        proceed = False
        for prompts in TAGPROMPTS.values():
            if message.content.startswith(prompts.get(TAGS[EMAILADDRESS], TAGPROMPTS[GENERAL][TAGS[EMAILADDRESS]])):
                proceed = True
                break
        if not proceed:
            return 0
        def preprocessing(text):
            '''
            do any text preprocessing here!
            '''
            return True
        failed_preprocessing_string = ''
        selected = await UI.preprocess_reaction_selection_menu(client, message, user, 1)
        if selected:
            for ind, emoji in GENERAL_OPTION_EMOJIS.items():
                if selected[0].emoji == emoji:
                    selected[0] = 'done'
                    msg = await message.channel.fetch_message(message.channel.last_message_id)
                    if msg != message:
                        if preprocessing(msg.content):
                            prompter_response = await UI.prompter(client, message.channel, TAGS[EMAILADDRESS], msg.content, reactor=user)
                            if prompter_response != PROMPTERDIDNOTHING:
                                await message.delete()
                                await msg.delete()
                                return prompter_response
                            await message.clear_reactions()
                        else:
                            await msg.delete()
                            await message.channel.send(failed_preprocessing_string)
                    return 1
        return 0
    
    async def prompt_email_confirmation(func_enum, channel):
        options = ['finish']
        await UI.prompt_option_from_option_list_from_user(channel, TAGPROMPTS.get(func_enum, TAGPROMPTS[GENERAL])[TAGS[EMAILCONFIRMATION]], options, 10)
        
    async def process_email_confirmation(client, message, user):
        assert client.user == message.author, "This isn't an inquiry sent by our bot. Do better code."
        proceed = False
        for prompts in TAGPROMPTS.values():
            if message.content.startswith(prompts.get(TAGS[EMAILCONFIRMATION], TAGPROMPTS[GENERAL][TAGS[EMAILCONFIRMATION]])):
                proceed = True
                break
        if not proceed:
            return 0
        def preprocessing(text):
            '''
            do any text preprocessing here!
            '''
            return True
        failed_preprocessing_string = ''
        selected = await UI.preprocess_reaction_selection_menu(client, message, user, 1)
        if selected:
            for ind, emoji in GENERAL_OPTION_EMOJIS.items():
                if selected[0].emoji == emoji:
                    selected[0] = 'done'
                    msg = await message.channel.fetch_message(message.channel.last_message_id)
                    if msg != message:
                        if preprocessing(msg.content):
                            prompter_response = await UI.prompter(client, message.channel, TAGS[EMAILCONFIRMATION], msg.content, reactor=user)
                            if prompter_response != PROMPTERDIDNOTHING:
                                await message.delete()
                                await msg.delete()
                                return prompter_response
                            await message.clear_reactions()
                        else:
                            await msg.delete()
                            await message.channel.send(failed_preprocessing_string)
                    return 1
        return 0
    
    async def prompt_username(func_enum, channel):
        options = ['finish']
        await UI.prompt_option_from_option_list_from_user(channel, TAGPROMPTS.get(func_enum, TAGPROMPTS[GENERAL])[TAGS[USERNAME]], options, 10)
        
    async def process_username(client, message, user):
        assert client.user == message.author, "This isn't an inquiry sent by our bot. Do better code."
        proceed = False
        for prompts in TAGPROMPTS.values():
            if message.content.startswith(prompts.get(TAGS[USERNAME], TAGPROMPTS[GENERAL][TAGS[USERNAME]])):
                proceed = True
                break
        if not proceed:
            return 0
        def preprocessing(guild, text):
            '''
            do any text preprocessing here!
            '''
            user = get_member_by_discord_name(guild, text)
            return user != False
        
        failed_preprocessing_string = 'We couldn\'t find that user in this server at all! Try again!'
        selected = await UI.preprocess_reaction_selection_menu(client, message, user, 1)
        if selected:
            for ind, emoji in GENERAL_OPTION_EMOJIS.items():
                if selected[0].emoji == emoji:
                    selected[0] = 'done'
                    msg = await message.channel.fetch_message(message.channel.last_message_id)
                    if msg != message:
                        if preprocessing(message.channel.guild, msg.content):
                            prompter_response = await UI.prompter(client, message.channel, TAGS[USERNAME], msg.content, reactor=user)
                            if prompter_response != PROMPTERDIDNOTHING:
                                await message.delete()
                                await msg.delete()
                                return prompter_response
                            await message.clear_reactions()
                        else:
                            await msg.delete()
                            await message.channel.send(failed_preprocessing_string, delete_after=3)
                    return 1
        return 0
    
    async def prompt_text_channel(func_enum, channel):
        cat = channel.guild.get_channel(channel.category_id)
        channel_names = cat.text_channels
        for ind, chan in enumerate(channel_names):
            channel_names[ind] = chan.name
        await UI.prompt_option_from_option_list_from_user(channel, TAGPROMPTS.get(func_enum, TAGPROMPTS[GENERAL])[TAGS[TEXTCHANNEL]], channel_names, 15)
    
    async def process_text_channel(client, message,user):
        assert client.user == message.author, "This isn't an inquiry sent by our bot. Do better code."
        proceed = False
        for prompts in TAGPROMPTS.values():
            if message.content.startswith(prompts.get(TAGS[TEXTCHANNEL], TAGPROMPTS[GENERAL][TAGS[TEXTCHANNEL]])):
                proceed = True
                break
        if not proceed:
            return 0
        options = message.channel.category.text_channels
        selected = await UI.preprocess_reaction_selection_menu(client, message, user, len(message.channel.category.text_channels))
        if selected:
            for ind, emoji in GENERAL_OPTION_EMOJIS.items():
                if selected[0].emoji == emoji:
                    selected[0] = options[ind]
                    break
            await message.edit(content = 'Gotcha! I\'ll do ' + selected[0].name)
            await message.clear_reactions()
            prompter_response = await UI.prompter(client, message.channel, TAGS[TEXTCHANNEL], selected[0].name, reactor=user)
            if prompter_response != PROMPTERDIDNOTHING:
                await message.delete()
                return prompter_response
        return selected
    
    async def prompt_clubname(func_enum, channel):
        channel_names = list(ClubSuite.get_all_clubs(channel.guild))
        for ind, chan in enumerate(channel_names):
            channel_names[ind] = chan.name
        await UI.prompt_option_from_option_list_from_user(channel, TAGPROMPTS.get(func_enum, TAGPROMPTS[GENERAL])[TAGS[CLUBNAME]], channel_names, 8)
    
    async def process_clubname(client, message,user):
        assert client.user == message.author, "This isn't an inquiry sent by our bot. Do better code."
        proceed = False
        for prompts in TAGPROMPTS.values():
            if message.content.startswith(prompts.get(TAGS[CLUBNAME], TAGPROMPTS[GENERAL][TAGS[CLUBNAME]])):
                proceed = True
                break
        if not proceed:
            return 0
        options = list(ClubSuite.get_all_clubs(message.channel.guild))
        for ind, option in enumerate(options):
            options[ind] = option.name
        
        selected = await UI.preprocess_reaction_selection_menu(client, message, user, len(options))
        if selected:
            for ind, emoji in GENERAL_OPTION_EMOJIS.items():
                if selected[0].emoji == emoji:
                    selected = options[ind]
                    break
            '''
                Return here to move message movement feature from control panel into this one
            '''
            if type(selected) == list:
            # get the current page out of the message_board
                page_num = ""
                i,j=(0,0)
                for i in range(len(message.content)):
                    if message.content[i] == "(":
                        j = i + 1
                        while message.content[j] != "/":
                            page_num += message.content[j]
                            j += 1
                        break
                            
                func_enum = None
                for msg in await message.channel.pins():
                    if msg.author == client.user and msg.content.startswith(TAGS[PROMPTERSETUP]):
                        func_enum = int(msg.content[msg.content.index('(') + 1: msg.content.index(')')])
                        break
                page_num = int(page_num)
                if selected[0].emoji == UI.NEXT_BUTTON:
                    await UI.prompt_option_from_option_list_from_user(message.channel, TAGPROMPTS.get(func_enum, TAGPROMPTS[GENERAL])[TAGS[CLUBNAME]], options, 8, page=page_num + 1, message=message)
                elif selected[0].emoji == UI.PREV_BUTTON:
                    await UI.prompt_option_from_option_list_from_user(message.channel, TAGPROMPTS.get(func_enum, TAGPROMPTS[GENERAL])[TAGS[CLUBNAME]], options, 8, page=page_num - 1, message=message)
                return
        await message.edit(content = 'Gotcha! I\'ll do ' + selected)
        await message.clear_reactions()
        prompter_response = await UI.prompter(client, message.channel, TAGS[CLUBNAME], selected, reactor=user)
        if prompter_response != PROMPTERDIDNOTHING:
            await message.delete()
            return prompter_response
        return PROMPTERDIDNOTHING
    
    async def prompt_voice_channel(func_enum, channel):
        cat = channel.guild.get_channel(channel.category_id)
        channel_names = cat.voice_channels
        for ind, chan in enumerate(channel_names):
            channel_names[ind] = chan.name
        await UI.prompt_option_from_option_list_from_user(channel, TAGPROMPTS.get(func_enum, TAGPROMPTS[GENERAL])[TAGS[VOICECHANNEL]], channel_names, 15)
    
    async def process_voice_channel(client, message,user):
        assert client.user == message.author, "This isn't an inquiry sent by our bot. Do better code."
        proceed = False
        for prompts in TAGPROMPTS.values():
            if message.content.startswith(prompts.get(TAGS[VOICECHANNEL], TAGPROMPTS[GENERAL][TAGS[VOICECHANNEL]])):
                proceed = True
                break
        if not proceed:
            return 0
        options = message.channel.category.voice_channels
        selected = await UI.preprocess_reaction_selection_menu(client, message, user, len(options))
        if selected:
            for ind, emoji in GENERAL_OPTION_EMOJIS.items():
                if selected[0].emoji == emoji:
                    selected[0] = options[ind]
                    break
            await message.edit(content = 'Gotcha! I\'ll do ' + selected[0].name)
            await message.clear_reactions()
            prompter_response = await UI.prompter(client, message.channel, TAGS[VOICECHANNEL], selected[0].name, reactor=user)
            if prompter_response != PROMPTERDIDNOTHING:
                await message.delete()
                return prompter_response
        return selected
    
    
    async def prompt_weekday(func_enum, channel):
        await UI.prompt_option_from_option_list_from_user(channel, TAGPROMPTS.get(func_enum, TAGPROMPTS[GENERAL])[TAGS[WEEKDAY]], ['Monday','Tuesday', 'Wednesday','Thursday','Friday','Saturday','Sunday'], 10)
    
    async def process_weekday(client, message, user):
        assert client.user == message.author, "This isn't an inquiry sent by our bot. Do better code."
        
        proceed = False
        for prompts in TAGPROMPTS.values():
            if message.content.startswith(prompts.get(TAGS[WEEKDAY], TAGPROMPTS[GENERAL][TAGS[WEEKDAY]])):
                proceed = True
                break
        if not proceed:
            return 0
        
        options = ['Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday']
        selected = await UI.preprocess_reaction_selection_menu(client, message, user, len(options))
        if selected:
            for ind, emoji in GENERAL_OPTION_EMOJIS.items():
                if selected[0].emoji == emoji:
                    selected[0] = options[ind]
                    break
            await message.edit(content = 'Gotcha! The weekday %s is great' % selected[0])
            prompter_response = await UI.prompter(client, message.channel, TAGS[WEEKDAY], selected[0], reactor=user)
            if prompter_response != PROMPTERDIDNOTHING:
                await message.delete()
                return prompter_response
            await message.clear_reactions()
        return selected
    
    async def prompt_time(func_enum, channel):
        options_fields = {'hour':[str(i) for i in range(1, 24)], 'minute':[str(i) for i in range(0, 60,15)], 'am/pm':['am', 'pm']}
        await UI.prompt_rotate_option_field(channel, TAGPROMPTS.get(func_enum, TAGPROMPTS[GENERAL])[TAGS[TIME]], options_fields)
        
    # assumes that the client sent the message
    async def process_time(client, message, user):
        assert client.user == message.author, "This isn't an inquiry sent by our bot. Do better code."
        proceed = False
        for prompts in TAGPROMPTS.values():
            if message.content.startswith(prompts.get(TAGS[TIME], TAGPROMPTS[GENERAL][TAGS[TIME]])):
                proceed = True
                break
        if not proceed:
            return 0
        
        options_fields = {'hour':[str(i) for i in range(1, 24)], 'minute':[str(i) for i in range(0, 60,15)], 'am/pm':['am', 'pm']}
        time_result = await UI.process_rotate_option_field(client,message, user, options_fields, "Thanks! The time %s:%s%s sounds good to me!")
        if type(time_result) == dict:
            prompter_resopnse = await UI.prompter(client, message.channel, TAGS[TIME], '%s:%s%s' % tuple(time_result.values()), reactor=user)
            if prompter_response != PROMPTERDIDNOTHING:
                await message.delete()
                return prompter_response
    
    async def prompt_date(func_enum, channel):
        options_fields = {'day':[str(i) for i in range(1, 32)], 'month':[str(i) for i in range(1, 13)], 'year':[2020, 2021]}
        await UI.prompt_rotate_option_field(channel, TAGPROMPTS.get(func_enum, TAGPROMPTS[GENERAL])[TAGS[DATE]], options_fields)
        
    # assumes that the client sent the message
    async def process_date(client, message, user):
        assert client.user == message.author, "This isn't an inquiry sent by our bot. Do better code."
        proceed = False
        for prompts in TAGPROMPTS.values():
            if message.content.startswith(prompts.get(TAGS[DATE], TAGPROMPTS[GENERAL][TAGS[DATE]])):
                proceed = True
                break
        if not proceed:
            return 0
        options_fields = {'day':[str(i) for i in range(1, 32)], 'month':[str(i) for i in range(1, 13)], 'year':['2020', '2021']}
        date_result = await UI.process_rotate_option_field(client,message, user, options_fields, "Thanks! The day %s/%s/%s sounds good to me!")
        if type(date_result) == dict:
            prompter_response = await UI.prompter(client, message.channel, TAGS[DATE], '%s/%s/%s' % tuple(date_result.values()), reactor=user)
            if prompter_response != PROMPTERDIDNOTHING:
                await message.delete()
                return prompter_response
    
    # options_fields is a dict of lists in {field_name: [options]} formate
    async def prompt_rotate_option_field(channel, prompt, options_fields):
        options = []
        message_text = prompt + '\n\r'
        for field_name, options_ in options_fields.items():
            message_text += field_name + '| ' + str(options_[0]) + '\n\r'
            options.append(' next ' + str(field_name))
        options.append('done')
        await UI.prompt_option_from_option_list_from_user(channel, message_text, options, 10)
    
    async def process_rotate_option_field(client, message, user, options_field, done_message, emoji_control_options=GENERAL_OPTION_EMOJIS):
        # Assumes user selected an actual option. 
        async def rotate_field(reaction, message, user, options_fields, emoji_control_options):
            field_name = None
            emoji = reaction.emoji
            # Get field to rotate
            for indx, cemoji in emoji_control_options.items():
                if emoji == cemoji:
                    field_name = list(options_fields.keys())[indx]
                    break
            new_message_content = ''
            for line in message.content.split('\n\r'):
                if line.startswith(field_name + "| "):
                    current_value = line[len(field_name + '| '):]
                    next_value = options_fields[field_name].index(current_value)
                    next_value = options_fields[field_name][(next_value + 1)%len(options_fields[field_name])]
                    line = line[:len(field_name + '| ')] + str(next_value)
                new_message_content += line + '\n\r'
            await message.edit(content = new_message_content)
            await reaction.remove(user)
            return 1
            
        async def done_button(reaction, message, options_fields, emoji_control_options, done_message):
            done_button = emoji_control_options[len(options_fields.keys())]
            if reaction.emoji != done_button:
                return False
            chosen_vals = {}
            for line in message.content.split('\n\r'):
                if '|' in line:
                    key, val = line.split('| ')
                    chosen_vals[key] = val
            await message.edit(content=done_message % tuple(chosen_vals.values()))
            await message.clear_reactions()
            return chosen_vals
        
        selected = await UI.preprocess_reaction_selection_menu(client, message, user, len(options_field.keys()))
        for selection in selected:
            resp = await done_button(selection, message, options_field, emoji_control_options, done_message)
            if resp:
                return resp
            await rotate_field(selection, message, user, options_field, emoji_control_options)
            return UI.SUCCESSFULLY_ROTATED_OPTION
        return UI.INVALID_CHOSEN_OPTION
    
    async def __process_payload(client, payload):
        try:
            guild = client.get_guild(payload.guild_id)
        except:
            return -1
        
        channel = guild.get_channel(payload.channel_id)
        message = await channel.fetch_message(payload.message_id)
        user = guild.get_member(payload.user_id)
        return [message, user, payload.emoji]
    
    async def text_lister(channel, func_enum, message_type, page=1, emojis = GENERAL_OPTION_EMOJIS, message = None, close_button = True):
        max_at_a_time = TextLister.MAX_AT_A_TIMES[func_enum]
        text_to_list = await TEXTGENERATORS[func_enum](channel)
        text = message_type + '-' + str(func_enum) + ' ' + TEXTPROMPTS[func_enum]
        pages = len(text_to_list)//max_at_a_time + 1
        
        
        
        if message_type == 'T':      
            button_options = TextLister.BUTTONS[func_enum].values()
        else:
            button_options = []
        
        if len(text_to_list) > max_at_a_time:
            text += " (" + str(page) + "/" + str(pages) + ')'
        else:
            text += ' '
        text += ')\n'
        for ind, option in enumerate(text_to_list):
            if (page - 1) * max_at_a_time <= ind < max_at_a_time * page:
                text += option + '\n'
        for ind, option in enumerate(button_options):
            text += str(emojis[ind]) + ' for ' + str(option) + '\n'
        
        # Add message controls to the server.
        additional_reactions = []
        if pages > 1:
            if page != 1:
                text += UI.PREV_BUTTON + ' for previous options.'
                additional_reactions.append(UI.PREV_BUTTON)
            if page != pages:
                text += UI.NEXT_BUTTON + ' for more options.\n'
                additional_reactions.append(UI.NEXT_BUTTON)
        if close_button:
            text += UI.CLOSE_BUTTON + ' to close.'
            additional_reactions.append(UI.CLOSE_BUTTON)
        # Add ui buttons depending on the page counts.
        
        if message == None:
            message = await channel.send(text)
        else:
            await message.edit(content = text)
            await message.clear_reactions()
        
        for i in range(len(button_options)):
            await message.add_reaction(emojis[i])
        for rxn in additional_reactions:
            await message.add_reaction(rxn)
    
    async def process_text_lister(client, message, user, emojis=GENERAL_OPTION_EMOJIS):
        if not message.content.startswith('T-'): # Not a text prompt
            return 0
        func_enum = int(message.content[2:message.content.index(' ')])
        selected = await UI.preprocess_reaction_selection_menu(client, message, user, 1)
        for ind, emoji in enumerate(selected):
            emoji = emoji.emoji
            for key, value in emojis.items():
                #print(emoji.emoji, repr(value))
                if emoji == value:
                    selected = list(TextLister.BUTTONS[func_enum].values())[key]
                    break
        if type(selected) == list:
            # get the current page out of the message_board
            page_num = ""
            i,j=(0,0)
            for i in range(len(message.content)):
                if message.content[i] == "(":
                    j = i + 1
                    while message.content[j] != "/":
                        page_num += message.content[j]
                        j += 1
                    break
            page_num = int(page_num)
            if selected[0].emoji == UI.NEXT_BUTTON:
                await UI.text_lister(message.channel, func_enum, message.content[0], page=page_num + 1, message=message)
            elif selected[0].emoji == UI.PREV_BUTTON:
                await UI.text_lister(message.channel, func_enum, message.content[0], page=page_num - 1, message=message)
            elif selected[0].emoji == UI.CLOSE_BUTTON:
                await message.delete()
            return
        
        return (await TextLister.click(client, message.channel, func_enum, selected), selected)
    '''
    if message, then this will make edit the given message to turn it into a selection board.
    '''
    async def prompt_option_from_option_list_from_user(channel, prompt, options, max_at_a_time, page=1, emojis = GENERAL_OPTION_EMOJIS, message = None):
        assert max_at_a_time >= 3, "Too few max options!"
        text = prompt
        pages = len(options)//max_at_a_time + 1
        if len(options) > max_at_a_time:
            text += "(" + str(page) + "/" + str(pages) + ')'
        text += '\n\t'
        for ind, option in enumerate(options):
            if max_at_a_time * (page - 1) <= ind < max_at_a_time * page:
                text += str(emojis[ind]) + ' for ' + str(option) + '\n\t'
        
        
        additional_reactions = []
        if pages > 1:
            if page != 1:
                text += UI.PREV_BUTTON + ' for previous options.'
                additional_reactions.append(UI.PREV_BUTTON)
            if page != pages:
                text += UI.NEXT_BUTTON + ' for more options.\n\t'
                additional_reactions.append(UI.NEXT_BUTTON)
            
        # Add ui buttons depending on the page counts.
        
        if message == None:
            message = await channel.send(text)
        else:
            await message.edit(content = text)
            await message.clear_reactions()
            

        for i in range((page-1)*max_at_a_time, min(len(options), page*max_at_a_time)):
            await message.add_reaction(emojis[i])
        for rxn in additional_reactions:
            await message.add_reaction(rxn)
    # Returns list selected: [reaction]
    async def preprocess_reaction_selection_menu(client, message, user, max_options_selectable):
        if message.author != client.user:
            return -1
        reactions = message.reactions
        selected = []
        remove_from_user = False
        for reaction in reactions:
            if client.user not in await reaction.users().flatten():  # make sure this is actually a real option and not something someone just threw on the option board
                await reaction.remove(user)
            if reaction.count == 2: # Get selected emojis
                if len(selected) >= max_options_selectable: # This is for the max number of emojis you want selected. Some people want to select an hour, a minute, and am/pm, which is 3, for example
                    remove_from_user = True
                selected.append(reaction)
            elif reaction.count == 3: # make sure you can't over-select an emoji.
                await reaction.remove(user)
        if remove_from_user: # and deal with over the max if it comes up.
            for reaction in selected:
                await reaction.remove(user)
                selected.remove(reaction)
        if len(selected) == max_options_selectable:
            for selection in selected:
                await selection.remove(user)
        return selected # Successfully preprocessed the command board.
    
    async def process_control_panel_reaction(client, message, user, emojis = GENERAL_OPTION_EMOJIS):
        assert client.user == message.author, "This isn't an inquiry sent by our bot. Do better code."
        if not message.content[3:len(ControlPanel.STARTING_TEXT) + 3] == ControlPanel.STARTING_TEXT:
            return 0 # not this function.
        
        selected = await UI.preprocess_reaction_selection_menu(client, message, user, 1)
        # There should only ever be 1 selected emoji returned at a time.
        ctrl_panel = None
        if message.content.startswith(Club.channel_type.upper()):
            ctrl_panel = ClubControlPanel
        elif message.content.startswith(PublicChan.channel_type.upper()):
            ctrl_panel = OpenControlPanel
        elif message.content.startswith(RestrictChan.channel_type.upper()):
            ctrl_panel = RestrictControlPanel
        elif message.content.startswith(AdminChannel.channel_type.upper()):
            ctrl_panel = AdminControlPanel
        elif message.content.startswith(AuthControlPanel.SIGNATURE):
            ctrl_panel = AuthControlPanel
        elif message.content.startswith(GeneralControlPanel.SIGNATURE):
            ctrl_panel = GeneralControlPanel
        
        for ind, emoji in enumerate(selected):
            emoji = emoji.emoji
            for key, value in emojis.items():
                #print(emoji.emoji, repr(value))
                if emoji == value:
                    selected = list(ctrl_panel.BUTTONS.values())[key]
                    break
        if type(selected) == list:
            # get the current page out of the message_board
            page_num = ""
            i,j=(0,0)
            for i in range(len(message.content)):
                if message.content[i] == "(":
                    j = i + 1
                    while message.content[j] != "/":
                        page_num += message.content[j]
                        j += 1
                    break
                        
            page_num = int(page_num)
            if selected[0].emoji == UI.NEXT_BUTTON:
                await UI.prompt_option_from_option_list_from_user(message.channel, message.content[:3] + "Click a button to do something!", ctrl_panel.BUTTONS.values(), ControlPanel.MAX_AT_A_TIME, page=page_num + 1, message=message)
            elif selected[0].emoji == UI.PREV_BUTTON:
                await UI.prompt_option_from_option_list_from_user(message.channel, message.content[:3] + "Click a button to do something!", ctrl_panel.BUTTONS.values(), ControlPanel.MAX_AT_A_TIME, page=page_num - 1, message=message)
            return
        
        return (await ctrl_panel.click(client, message.channel, selected), selected)
    async def prompter(client, channel, target_field, substitution, reactor=None, after=None):
        if after == None:
            after = datetime(1,1,1)
            
        for message in await channel.pins():
            if message.author == client.user and message.content.startswith(TAGS[PROMPTERSETUP]):
                func_enum = int(message.content[message.content.index('(') + 1: message.content.index(')')])
                parts = message.content.split(' ')
                for i in range(len(parts)):
                    if target_field in parts[i]:
                        parts[i] = '[' + str(substitution) + ']'
                        await message.edit(content=' '.join(parts))
                    elif PROMPTS.get(parts[i], 0):
                        await PROMPTS[parts[i]](func_enum, channel)
                        return PROMPTERPROMTED
                await message.delete()
                return await ControlPanel.process_finished_prompt(func_enum, channel, reactor, ' '.join(parts[2:]))
        return PROMPTERDIDNOTHING
    
    async def prompt(client, func_enum, channel):
        await (await channel.send(TAGS[PROMPTERSETUP] + ' (' + str(func_enum) + "): " + TAGSTRINGS[func_enum])).pin()
        await (await channel.fetch_message(channel.last_message_id)).delete()
        await UI.prompter(client, channel,'dummy','dummy')
        
    async def on_raw_reaction_add(client, payload):
        message, user, emoji = await UI.__process_payload(client, payload)
        if client.user != message.author or client.user == user:
            return (message, 4)
        response = await UI.process_control_panel_reaction(client, message, user)
        if response:
            return (message, 1)
        response = await UI.process_text(client, message, user)
        if response:
            return (message, 1)
        response = await UI.process_text_channel(client, message, user)
        if response:
            return (message, 1)
        response = await UI.process_voice_channel(client, message, user)
        if response:
            return (message, 1)
        response = await UI.process_email(client, message, user)        
        if response:
            return (message, 1)
        reponse = await UI.process_clubname(client, message, user)
        if response:
            return (message, 1)
        response = await UI.process_email_confirmation(client, message, user)
        if response:
            return (message, 1)
        response = await UI.process_username(client, message, user)
        if response:
            return (message, 1)
        response = await UI.process_time(client, message, user)
        if response:
            return (message, 1)
        response = await UI.process_weekday(client, message, user)
        if response:
            return (message, 1)
        response = await UI.process_date(client, message, user)
        if response:
            return (message, 1)
        response  = await UI.process_text_lister(client, message, user)
        if response:
            return (message, 1)
        return (message, 4)
    
PROMPTS = {TAGS[TEXT]:UI.prompt_text,
           TAGS[TEXTCHANNEL]:UI.prompt_text_channel,
           TAGS[VOICECHANNEL]:UI.prompt_voice_channel,
           TAGS[USERNAME]:UI.prompt_username,
           TAGS[EMAILADDRESS]:UI.prompt_email,
           TAGS[EMAILCONFIRMATION]:UI.prompt_email_confirmation,
           TAGS[TIME]:UI.prompt_time,
           TAGS[WEEKDAY]:UI.prompt_weekday,
           TAGS[DATE]:UI.prompt_date,
           TAGS[CLUBNAME]:UI.prompt_clubname,
    }