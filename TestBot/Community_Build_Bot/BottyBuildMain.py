# -*- coding: utf-8 -*-
"""
Created on Sat Sep  5 14:09:29 2020

@author: josep
"""

from handles import bot_token, MIN_BETWEEN_TASK_EXECUTIONS, get_member_by_discord_name
import discord
from discord.ext import commands, tasks
from datetime import datetime, timedelta
from asyncio import sleep

from PublicChannel import PubChanSuite
from ClubChannel import ClubSuite
from restrictedchannel import RestrictChanSuite
from AbstractChannel import AbstractChanSuite
from AdminChannel import AdminChanSuite, AdminChannel
from SOCChannel import SOCChannelSuite
from UI import UI, AuthControlPanel, GeneralControlPanel, AdminControlPanel

from Auth import User, ghelp, authenticate, send_auth_code_to_user, make_new_member, process_auth


from rssfeedtest import get_today_at_earlham
from Response_Handling import handle_response_codes

def get_member_by_discord_name(guild, discord_name):
    for member in guild.members:
        if member.name + "#" + member.discriminator == discord_name:
            return member
    return False

def is_auth(channel, auth_channel_name):
    return channel.name == auth_channel_name
    #for channel in guild.text_channels:
    #    if channel.name == auth_channel_name:
    #        return channel
    #return False
    



#client = discord.Client()
intents = discord.Intents.default()
intents.members = True

client = commands.Bot('!', intents=intents)  





@tasks.loop(minutes=MIN_BETWEEN_TASK_EXECUTIONS)
async def do_scheduled_tasks():
    #print('started processing tasks')
    for guild in client.guilds:
        clis = AbstractChanSuite.get_all_clis(guild)
        try:
            await ClubSuite.process_tasks(clis)
            await RestrictChanSuite.process_tasks(clis)
            await PubChanSuite.process_tasks(clis)
            await AdminChanSuite.process_tasks(clis)
            await SOCChannelSuite.process_tasks(clis)
        except Exception as e:
            print(e)

@do_scheduled_tasks.before_loop
async def wait_until_multiple_of_15_min():
    print('Started before loop of server task scheduling service')
    now = datetime.now()
    future = datetime(now.year, now.month, now.day, now.hour + ((now.minute//15 + 1)==4), ((now.minute//15 + 1) * 15)%60, 0, 0)
    #future = datetime(now.year, now.month, now.day, now.hour + (not (now.minute + 1) % 60), ((now.minute) + 1)%60, 0,0)
    print('waiting to start server task scheduling service (s):',(future - now).seconds , 'or in minutes: ', (future-now).seconds//60)
    await sleep((future - now).seconds)
    #print('sleeping 10 seconds')
    #await sleep(10)

# I need a var section big time so i can call for updates
# THIS NEEDS PRIORITY UPDATES
@tasks.loop(hours=24)
async def update_today_at_schools():
    try:
        print('invoked update functionm')
        # ASSUME THE GUILD IS EARLHAM COLLEGe
        for guild in client.guilds:
            virtual_annoucements, local_annoucements = get_today_at_earlham()
            for channel in guild.channels:
                if channel.name == 'today-at-earlham':
                    await channel.send('Today at Earlham ' + str(datetime.today()) + ': \n\n')
                    for annoucement in local_annoucements:
                        await channel.send(annoucement)
                elif channel.name == 'today-at-remote-earlham':
                    await channel.send('Today at Remote Earlham ' + str(datetime.today()) + ': \n\n')
                    for annoucement in virtual_annoucements:
                        await channel.send(annoucement)
    except:
        return
        
@update_today_at_schools.before_loop
async def before_update_today_at_schools():
    print('started update before loop')
    hour = 8
    minute = 5
    await client.wait_until_ready()
    now = datetime.now()
    future = datetime(now.year, now.month, now.day, hour, minute, 0)
    #future = datetime.now() + timedelta(seconds=5)
    if now.hour >= hour and now.minute > minute:
        future += timedelta(days=1)
    print('finished before loop setup. Sleeping...')
    await sleep((future-now).seconds)
    
    
@client.event
async def on_guild_join(guild):
    if not AdminChanSuite.get_admincat(guild):
        admin = AdminChannel(guild, guild.owner)
        await admin._init()
        
    
@client.event
async def on_member_join(member):
    new_member = await make_new_member(member)
    if await AdminChanSuite.get_auth_enabled(member.guild) == False:
        await new_member.authenticate(new_member._authcode)

@client.event
async def on_raw_reaction_add(payload):
    return await handle_response_codes(*(await UI.on_raw_reaction_add(client, payload)))

@client.event
async def on_message(message):
    if message.author == client.user:
        return
    #await message.channel.send('received', delete_after=3)
    # Is it a command? if it's not a command, ignore. Otherwise, proceed.
    
    content = message.content.lower()
    if content.startswith('!'):
        '''
        Check if channel is server's auth channel. If so, allow auth commands.
        '''
        try: 
            maybe_auth = is_auth(message.channel, await AdminChanSuite.get_auth_channel_name(message.guild))#''' This will break the program later. message.channel can be a DM channel. find a way to make it so that that wont break the channel'''
            print("hello!!!!'")
            print('got auth channel: ' + str(maybe_auth))
        except:
            maybe_auth = False
            print('could not find auth channel')
        
        if maybe_auth:
            # all messages in auth channel should delete after 3 sec, no matter what.
            try:
                await message.delete(delay=8)
            except:
                pass
            # Get the user to do auth stuff with
            
            '''
            AUTH COMMANDS GO HERE
            '''
            auth_user = await process_auth(message.channel, message.author)
            if auth_user != 20000:
                print('made it to auth commands')
                #killer = create_task(kill_after_n(3, message)) # messages in this channel expire after 3 seconds
                if content.startswith('!auth'): # this allows users to put in their codes
                    await authenticate(message.channel, message.author, content[len('!auth '):].upper())
                elif content.startswith('!commandpanel'):
                    await AuthControlPanel._init_(message.channel)
                else: #anything else is ![prefix] which files an email attempt
                    await send_auth_code_to_user(message.channel, message.author, content[len('!'):])
                #run(killer)
            return 1
        
        '''
        Process any ADMIN commands if any-- commands that can be performed via the
        ADMIN CHANNEL structure.
        '''
        # try catch around this boy
        #response = AdminChanSuite.process_commands(message) # is a response number from ADminChanSUite.process_commands(message)
        #if response != 0:
        #
        print("attempting adminchansuiteprocess")
        response = await AdminChanSuite.process_commands(message)
        if response:
            return await handle_response_codes(message, response)
        if message.channel.id == AdminChanSuite.get_cli(AdminChanSuite.get_admincat(message.guild)).id:
            if message.content.startswith('!commandpanel'):
                return await AdminControlPanel._init_(message.channel)
            
        '''
        Check if the channel mentioned is a cli for a club. if so, perform club commands.
        '''
        # try catch around this boy
        response = await ClubSuite.process_commands(message)
        if response:
            return await handle_response_codes(message, response)
        
        '''
            GENERAL COMMANDS GO HERE
        '''
        
            # Find club category
            # if there is a role called category [member], add the person to the role]
        #elif content.startswith('!MAKESTUDENT'):
        #    student = ECStudent(get_member_by_discord_name(message.channel.guild, content[len('!MAKESTUDENT '):]))
        #    await student._init(role_name=student_role_name)
        #    return 
        if content.startswith('!commandpanel'):
            await GeneralControlPanel._init_(message.channel)
            return 1
        elif content.startswith('!examples'):
            commands = ['!clublist\n\t\t - lists all clubs in ' + message.channel.guild.name,
                        '!joinclub ECHackers\n\t\t - Joins the ECHackers Club',
                        '!leaveclub ECHackers\n\t\t - Leaves the ECHackers Club',
                        '!makeclub JelloLicking\n\t\t - Makes the JelloLicking club',
                        '!admin\n\t\t - (does nothing now) Opens a channel with you and admins', 
                        '!report person reason\n\t\t - (does nothing now) Sends a report on a person to an admin or club convener.',
                        '!addtextchannel Problems-with-admin Friend-Houses\n\t\t - Adds the Friend-Houses text channel to Problems-with-admin',
                        '!removetextchannel Problems-with-admin Friend-Houses\n\t\t - Removes the Friend-Houses text channel from Problems-with-admin (if you made it)',
                        '!addvoicechannel Problems-with-admin Friend-Houses\n\t\t - Adds the Friend-Houses voice channel to Problems-with-admin',
                        '!removevoicechannel Problems-with-admin Friend-Houses\n\t\t - Removes the Friend-Houses voice channel from Problems-with-admin (if you made it)',]
            send_text = 'Examples of commands ' + message.channel.guild.name + ' are (In order):\n\t'
            for command in commands:
                send_text += command + '\n\t'
            await message.channel.send(send_text)
            return 1
        elif content.startswith('!help') or content.startswith('!ocommands') or content.startswith('!ccommands') or content.startswith('!rcommands'):
            await ghelp(message.channel)
        
        elif content.startswith('!admin'):
            await message.channel.send('not yet implemented...')
            raise NotImplementedError()
        elif content.startswith('!report'):
            await message.channel.send('not yet implemented...')
            raise NotImplementedError()
        elif content.startswith('!whatcanido'):
            options = '''\n    -Have constructive conversations about topics you\'re passionate about
                - Play games with people you know and love from Earlham
                - Host Spaces for your friends to hang out with other people
                - Learn more about whats happening on Earlham\'s campus digitally
                - Do 24/7 options for meeting and greeting other Earlham Students
                - Connect with the Earlham Community 
                - And much much more!'''
            await message.channel.send('What can you do? You can:' + options)
        elif content.startswith('!whatwaycanidoit'):
            options = '''\n    - Click a text channel in the side bar to have a conversation
                - Click voice channel in the side bar to have a face to face conversation and/or share screens!
                - do !ocommands for ways to hang out with your Earlham friends
                - do !ccommands for ways to join, make, and interact with clubs in other ways
                - do !rcommands for ways to share your voice on impactful topics and do difference making!
                - do !help for general other commands, and !examples for how to use them
            '''
            await message.channel.send('What ways can do you things? You can:' + options)
        
        
        # Try to process remaining commands as hangoutspace commands:
        response = await PubChanSuite.process_commands(message)
        if response:
            return await handle_response_codes(message, response)
        
        # or as restricted suite petitions
        response = await RestrictChanSuite.process_commands(message)
        if response:
            return await handle_response_codes(message, response)
        
        
        
        # Always handle remaining response codes at the end.
        return await handle_response_codes(message, response)
    else:
        if message.content.startswith('<@!'):
            await ClubSuite.process_at_messages(message)
        try: 
            maybe_auth = is_auth(message.channel, await AdminChanSuite.get_auth_channel_name(message.guild))#''' This will break the program later. message.channel can be a DM channel. find a way to make it so that that wont break the channel'''
        except:
            print('could not find auth channel')
        if maybe_auth:
            await message.delete(delay=3)
            auth_user = await process_auth(message.channel, message.author)
    # Check if a message comes in from a CLI channel. 
    #   if in a CLI channel, 
        # is it an admin channel?
        # is it a convener channel?
        # is it a ____ channel?
        #Process according to context.
    
    # Check if the message is a general command. Process accordingly.
    
    
    # moderation
    # policing
# Down the line:
# In meet & greet lobbies, link to games, shows, 
# and movies after 15 min of people talking.
# We NEED to be able to host remote events. Convocation, Speaker events, and more
do_scheduled_tasks.start()
update_today_at_schools.start()
client.run(bot_token)
