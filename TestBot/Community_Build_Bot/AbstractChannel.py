# -*- coding: utf-8 -*-
"""
Created on Fri Sep 18 09:10:50 2020

@author: josep
"""
class AbstractChannel:# Abstract For club, restricted, and hangout category channel
            #    with CLUB MEMBER role read and write permissions for club members and CLI access for club convenors
    Member_abstract_permissions = None
    '''
    default_guild_role_perms is a dictionary passed in containing: {'read_messages':bool, 'send_messages':bool, 'connect':bool, 'speak':bool}
    member_role_perms is a dictionary passed in containing: {'read_messages':bool, 'send_messages':bool,'connect':bool, 'speak':bool}
    CLIUser_role_perms is a dictionary passed in containing: {'read_messages':bool, 'send_messages':bool,'connect':bool, 'speak':bool}
    '''
    def __init__(self, guild, CLIUser, channelName, channelType, CLIUserRoleName, MemberRole, CLIUserRoleColor, MemberRoleColor,
                 default_guild_role_perms, member_role_perms, CLIUser_role_perms):
        # Make a cli where commands from club conveners here can shape club below.
        # Have list of all voice and text channels owned by the club.
        # Can only influence voice/text channels owned by this club object.
        # Clubs are the only things that can act on the server. Conveners can act
        # on club objects.

        #Auto give at least one person convener access to the club.
        # Needs functionality to give other ECStudents convener roles.
        '''
        # ONLY MAKE IF THERE ARE NO DUPLICATES.
        '''
        self._guild = guild
        self._CLIUserRole = CLIUser
        self._channelName = channelType + "-" + channelName
        self._CLIUserRoleColor = CLIUserRoleColor # discord.color.Color() object
        self._memberRoleClor = MemberRoleColor
        self._MemberRole = MemberRole
        self._CLIUserRoleName = CLIUserRoleName
        self._CLIUser = CLIUser
        self._default_guild_role_perms = default_guild_role_perms
        self._member_role_perms = member_role_perms
        self._CLIUserRolePerms = CLIUser_role_perms
    async def _init(self, standard_member_role_names, suite=None): # standard_member_roles should include variations on validated member roles. Like ECStudent, ECFaculty
        # Make sure all standard roles are actually already there before calling this function.
        is_dupe = suite._assert_non_duplicate(self._guild, self._channelName) # This respects the suite that we are making a new channel from. It can't work with AbstractChanSuite._assert_non_duplicate. intended feature.jh
        if is_dupe != 1:
            return is_dupe # this is a duplicate and we need to reflect that.
        
            
        
        '''
        CREATE & ASSIGN ROLES. (Expandable to include mods, operators, etc.)
        '''
        # Create convenor role 
        self._CLIUserRole = await self._guild.create_role(name=self._channelName + " " + self._CLIUserRoleName, colour=self._CLIUserRoleColor)
        # assign convener role
        await self._CLIUser.add_roles(self._CLIUserRole)

        '''
        CREATE HOLDER FOR CLUB ASSOCIATED STUDENT ROLES
        '''            
        self._chan_roles = [self._CLIUserRole] + [role for role in self._guild.roles if role.name in standard_member_role_names]
        
        '''
        CREATE CATEGORY CHANNEL W/ CLUB ASSOCIATED STUDENT ROLES PARTICIPATION PRIVILEDGES
        '''
        # Create  channel
        self._category = await self._guild.create_category(name=self._channelName) # Instantiate default roles for club
        for role in self._guild.roles:
            await self._category.set_permissions(role, **self._default_guild_role_perms) 
            if role.name == 'auth':
                await self._category.set_permissions(role, read_messages=False, send_messages=False)
        for role in self._chan_roles:
            await self._category.set_permissions(role, **self._member_role_perms)# and member roles for club
        
        '''
        CREATE CATEGORY DEFAULT TEXT CHANNELS
        '''
        # Create CLI text channel
        self._cli = await self._category.create_text_channel(self._channelName + "-" + self._CLIUserRoleName + '-command-line')
        # And make it private to all but CLIUsers
        for role in self._guild.roles:
            await self._cli.set_permissions(role, read_messages=False, send_messages=False)
        await self._cli.edit(topic = 'Only ' + self._CLIUserRoleName + 's can use this channel')
        await self._cli.set_permissions(self._CLIUserRole, read_messages=True, send_messages=True)
        if suite == None:
            await self._cli.send(AbstractChanSuite.get_help_message(self._cli))       
        else:
            await self._cli.send(suite.get_help_message(self._cli))
        return 1
        # Conveners and members of this club can post in this channel. (Use textchannel.set_permissions)
    def __repr__(self):
        return self._cli_channel_id
    
class AbstractChanSuite:
    '''
    Checks to make a channel does not already exist.
    '''
    def _assert_non_duplicate(guild, channel_type, channel_name):
        for chan in AbstractChanSuite.get_all_chans(guild, channel_type):
            if chan.name == channel_name: # Then there this is become a duplicate. Do not finish instantiation.
                return -4
        return 1
    
    '''
    chanType is either c, o, or R at this time.
    '''
    def get_all_chans(guild, chanType): # returns list of club categories
        return filter(lambda channel: channel.name.startswith(chanType + '-') and not ('-command-line' in channel.name), guild.channels)
    
    '''
    Resolve club category from club category name
    '''
    def get_chan(chans, name):
        formatted = AbstractChanSuite.format_uinp(name)
        for chan in chans:
            if chan.name.lower() == formatted:
                return chan
        return None
    
    def get_chan_roles(chan):
        roles = []
        for role in chan.guild.roles:
            if role.name.startswith(chan.name):
                roles.append(role)
        return roles
    
    '''
    Resolve club role from club and full role name
    '''
    def get_role(chan, roleName):
        for role in chan.guild.roles:
            if role.name == roleName:
                return role
        return None
    
    def get_all_clis(guild):
        clis = []
        for category in guild.categories:
            for textchannel in category.text_channels:
                if '-command-line' in textchannel.name[-1*len('-command-line'):]:
                    clis.append(textchannel)
                    break
        return clis
    
    def get_cli(category, CLIUserRoleName):
        for channel in category.channels:
            if category.name.lower() + '-' + CLIUserRoleName.lower() + '-command-line' == channel.name.lower():
                return channel
        return False
    
    '''
    Returns club category that matches proposed cli if there is one.
    '''
    def is_cli(chans, check_channel, CLIUserRoleName):
        
        for chan in chans:
            if chan.name.lower() + '-' + CLIUserRoleName.lower() + '-command-line' == check_channel.name.lower():
                return chan
        return False

    '''
    Create a new role for the club.
    Club - a guild category
    role_name: a string of the new role name. Ex: Organizer
    permissions: Permissions item to give the role
    '''
    async def add_role(club, role_name, permissions):
        raise NotImplementedError('Not Applicable Here')
        #await club.guild.create_role(club.name + ' ' + role_name, permissions) # Unstable.

    async def add_role_to_user(chan, user, roleName):
        await user.add_roles(AbstractChanSuite.get_role(chan, roleName))
        
    # Add new convener to the channel
    async def add_CLIUser(chan, user, CLIUserRoleName):
        CLIUserRoleName = chan.name + ' ' + CLIUserRoleName
        await user.add_roles(AbstractChanSuite.get_role(chan, CLIUserRoleName))

    async def ban_member(self, member):
        # permaban member from the pubchannel
        # do not allow join if on a list, i guess
        pass
    def unban_member(self, member):
        # unban member from the pubchannel
        # remove from the list, i guess
        pass
    
    '''
    Adds a private voice channel for cli organizers only
    '''
    async def add_private_voice_chan(chan, channelName, CLIUserRoleName, CLIUserPermissions):
        CLIUserRoleName = chan.name + ' ' + CLIUserRoleName
        new_channel = await AbstractChanSuite.add_voice_channel(chan, channelName)
        for role in chan.guild:
            new_channel.set_permissions(role, read_messages=False, send_messages=False, connect=False, speak=False, view_channel=False)
        await new_channel.set_permissions(AbstractChanSuite.get_role(chan, CLIUserRoleName), CLIUserPermissions)
    
    async def add_private_text_chan(chan, channelName, CLIUserRoleName, CLIUserPermissions):
        CLIUserRoleName = chan.name + ' ' + CLIUserRoleName
        new_channel = await AbstractChanSuite.add_text_channel(chan, channelName)
        for role in chan.guild:
            new_channel.set_permissions(role, read_messages=False, send_messages=False, connect=False, speak=False, view_channel=False)
        await new_channel.set_permissions(AbstractChanSuite.get_role(chan, CLIUserRoleName), CLIUserPermissions)
    
    '''
    add text channel to resolved club category channel
    '''
    async def add_text_channel(chan, channelName):
        # add a text channel to this category.
        channelName = AbstractChanSuite.format_uinp(channelName)
        await chan.create_text_channel(channelName)
    
    async def remove_text_channel(chan, channel_name):
        #remove text channel from category
        print('removing text channel ' + channel_name + ' from ' + chan.name)
        channel_name = AbstractChanSuite.format_uinp(channel_name)
        for channel in chan.text_channels:
            if channel.name.lower() == channel_name:
                await channel.delete()
                return True
        return False
    
    def is_text_channel(chan, check_chan):
        check_chan = check_chan.lower()
        for chan in chan.text_channels:
            print(chan.name.lower(), check_chan)
            if chan.name.lower() == check_chan:
                return chan
        return -15
    
    async def remove_text_msg_from_chan(channel, message_text):
        async for text in channel.history(limit=100):
            if text.content == message_text:
                await text.delete()
                return True
        return False
    
    '''
    add text channel to resolved club category channel
    '''
    async def add_voice_channel(chan, channelName):
        #add voice channel to category
        channelName = AbstractChanSuite.format_uinp(channelName)
        await chan.create_voice_channel(channelName)
        
    async def remove_voice_channel(chan, channel_name):
        #remove voice channel from this category
        channel_name = AbstractChanSuite.format_uinp(channel_name)
        for channel in chan.voice_channels:
            if channel.name == channel_name:
                await channel.delete()
                return True
        return False
    
    async def get_text_channel(chan, channel_name):
        channel_name = channel_name.lower()
        #remove text channel from category
        for channel in chan.text_channels:
            if channel.name.lower() == channel_name:
                return channel
        return False
    
    async def delete_category(chan): # Provide your category
        # Ask "are you sure" in the discord cli. if yes, delete the category.
        # if no, don't. Return true or false if category was successfully/unsuccessfully deleted, respectively.
        # Remove the clubnameX roles from the discord.        
        
        '''
        TAKE LOG OF ALL PRIOR ACTIVITY FROM ALL EXISTING TEXT CHANNELS
        '''
        #TODO
        
        '''
        DELETE ASSOCIATED CHANNELS & CATEGORIES
        '''        
        for channel in chan.text_channels + chan.voice_channels:
            await channel.delete()
            
        
        '''
        DELETE ASSOCIATED CLUB ROLES
        '''
        for role in AbstractChanSuite.get_chan_roles(chan):
            await role.delete()
        
        await chan.delete()
    def format_uinp(inp):
        return inp.lower().replace(' ', '-')
    def get_chan_commands():
        raise NotImplementedError()
    def get_restrictchan_command_examples():
        raise NotImplementedError() 
    
    def get_help_message(channel):
        return "Not Implemented"
    
    async def process_cli_commands(message):
        raise NotImplementedError()
        
    async def process_user_commands(message):
        raise NotImplementedError()
    
    async def process_commands(message):
        return await AbstractChanSuite.process_cli_commands(message) or await AbstractChanSuite.process_user_commands(message)
    async def process_tasks(clis):
        raise NotImplementedError()