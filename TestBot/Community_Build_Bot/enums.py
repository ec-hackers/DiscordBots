# -*- coding: utf-8 -*-
"""
Created on Fri Nov  6 14:12:40 2020

@author: josep
"""
'''
1-100 is for club commands
101-200 is for omod commands
201-300 is for rmod commands
301-400 is for admin commands
401-500 is for general user commands
501-600 is for club member commands
10001- for system info
'''
from ClubChannel import ClubSuite
from PublicChannel import PubChanSuite
from AdminChannel import AdminChanSuite
from restrictedchannel import RestrictChanSuite
from Auth import ghelp, authenticate, send_auth_code_to_user
from handles import get_member_by_discord_name

CADDTEXTCHANNEL = 1; CREMOVETEXTCHANNEL = 2; CADDVOICECHANNEL = 3;
CREMOVEVOICECHANNEL = 4; CCLOSECLUB = 5; CKICK = 6; CCONVENERHELP = 7;
CDESCRIBE = 8; CPIN = 9; CUNPIN = 10; CREMOVEMESSAGE = 11; CSCHEDULEMESSAGE = 12;
CUNSCHEDULEMESSAGE = 13; CWEEKLYREMINDER = 14; CUNSCHEDULEWEEKLYREMINDER=15;
CSETUPEMAIL=16; CREQUESTBOT=17;CADDMEMBER=18; CADDCONVENER=19

OREMOVEMESSAGE=101

OOADDTEXTCHANNEL=700;OOREMOVETEXTCHANNEL=701;OOADDVOICECHANNEL=702;OOREMOVEVOICECHANNEL=703;
RRADDTEXTCHANNEL=800;RREMOVETEXTCHANNEL=801;RRADDVOICECHANNEL=802;RREMOVEVOICECHANNEL=803;


RADDTEXTCHANNEL=201;RREMOVETEXTCHANNEL=202;RADDVOICECHANNEL=203;RREMOVEVOICECHANNEL=204;
RREMOVEMESSAGE=205

AADDPUBCHAN=301;AADDRESTRICTCHAN=302;ASETSENDERNAMEAS=303;ASETAUTHROLE0AS=304;
ASETAUTHROLE1AS=305;ASETDESTDOMAINAS=306;ATOGGLEAUTH=307;AUPDATE=308
ASETAUTHCHANNAMEAS=309;ASETWELCOMEMESSAGEAS=310

GDESCRIBECLUB=401;GJOINCLUB=402;GLEAVECLUB=403;GCLUBLIST=404;GMAKECLUB=405;
GOADDTEXTCHANNEL=406;GOREMOVETEXTCHANNEL=407;GOADDVOICECHANNEL=408;GOREMOVEVOICECHANNEL=409;
GRADDTEXTCHANNEL=410;GRREMOVETEXTCHANNEL=411;GRREMOVETEXTCHANNEL=412;GRREMOVEVOICECHANNEL=413;
GEXAMPLES=414;GHELP=415;GADMIN=416;GREPORT=417;GWHATCANIDO=418;GWHATWAYCANIDOIT=419;

UAUTHENTICATE = 601; USENDAUTHEMAIL=602;

TEXT = 10001;TEXTCHANNEL=10002;VOICECHANNEL=10012; USERNAME=10003; EMAILADDRESS=10004;TIME=10005;
WEEKDAY=10006; DATE=10007; CLUBNAME=10008; GENERAL=10009;EMAILCONFIRMATION=10010#DEFAULT=10009;

PROMPTERSETUP=10011;PROMPTERDIDNOTHING=10012;PROMPTERPROMTED=10013

'''
RESPONSE CODES
'''

UCONTROLPANELADDITIONSUCCESS = 30000;CCONTROLPANELADDITIONSUCCESS = 30001;RCONTROLPANELADDITIONSUCCESS = 30002;
OCONTROLPANELADDITIONSUCCESS = 30003;ACONTROLPANELADDITIONSUCCESS = 30004;
GCONTROLPANELADDITIONSUCCESS = 300011;
UCONTROLPANELCLICKED = 30005;CCONTROLPANELCLICKED = 30006;RCONTROLPANELCLICKED = 30007;
OCONTROLPANELCLICKED = 30008;ACONTROLPANELCLICKED = 30009;
GCONTROLPANELCLICKED = 30010;


CONTROLMAP={CADDTEXTCHANNEL:ClubSuite.add_text_channel,
            CREMOVETEXTCHANNEL:ClubSuite.remove_text_channel,
            CADDVOICECHANNEL:ClubSuite.add_voice_channel,
            CREMOVEVOICECHANNEL:ClubSuite.remove_voice_channel,
            CCLOSECLUB:ClubSuite.delete_category,
            CKICK:ClubSuite.ui_kick_member,
            CADDMEMBER:ClubSuite.ui_add_member,
            CADDCONVENER: ClubSuite.add_convener,
            CCONVENERHELP:ClubSuite.ui_convenerhelp,
            CDESCRIBE:ClubSuite.ui_describe,
            CPIN:ClubSuite.ui_pin,
            CUNPIN:ClubSuite.ui_unpin,
            CREMOVEMESSAGE:ClubSuite.remove_text_msg_from_chan,
            CSCHEDULEMESSAGE:ClubSuite.ui_schedule_message,
            CUNSCHEDULEMESSAGE:ClubSuite.ui_unschedule,
            CSETUPEMAIL:ClubSuite.ui_setup_email,
            CWEEKLYREMINDER:ClubSuite.ui_weekly_reminder,
            CUNSCHEDULEWEEKLYREMINDER:ClubSuite.ui_cancel_weekly_reminder,
            CREQUESTBOT:ClubSuite.ui_requestbot,
            
            GDESCRIBECLUB:ClubSuite.ui_describeclub,
            GJOINCLUB:ClubSuite.add_member,
            GLEAVECLUB:ClubSuite.kick_member,
            GCLUBLIST:GCLUBLIST,
            GMAKECLUB:ClubSuite.makeClub,
            GHELP:ghelp,
            
            UAUTHENTICATE: authenticate,
            USENDAUTHEMAIL: send_auth_code_to_user,
            
            OOADDTEXTCHANNEL: lambda x: x,#PubChanSuite.
            
            AADDPUBCHAN: AdminChanSuite.ui_add_pubchan,
            AADDRESTRICTCHAN: AdminChanSuite.ui_add_restrictchan,
            ASETSENDERNAMEAS: AdminChanSuite.ui_set_sender_name_as,
            ASETAUTHROLE0AS: AdminChanSuite.ui_set_authrole0_as,
            ASETAUTHROLE1AS: AdminChanSuite.ui_set_authrole1_as,
            ASETAUTHCHANNAMEAS: AdminChanSuite.ui_set_auth_chan_name_as,
            ASETDESTDOMAINAS: AdminChanSuite.ui_set_domain_dest_as,
            ATOGGLEAUTH: AdminChanSuite.ui_toggle_auth,
            ASETWELCOMEMESSAGEAS: AdminChanSuite.ui_set_welcome_message_as,
            AUPDATE: AdminChanSuite.ui_update
            }

WRAPPERMAP = {CADDTEXTCHANNEL: lambda Text=None, channel=None, user=None: CONTROLMAP[CADDTEXTCHANNEL](channel.guild.get_channel(channel.category_id), Text),
              CREMOVETEXTCHANNEL: lambda TextChannel=None, channel=None, user=None: CONTROLMAP[CREMOVETEXTCHANNEL](channel.guild.get_channel(channel.category_id), TextChannel),
              CADDVOICECHANNEL: lambda Text=None, channel=None, user=None: CONTROLMAP[CADDVOICECHANNEL](channel.guild.get_channel(channel.category_id), Text),
              CREMOVEVOICECHANNEL: lambda VoiceChannel=None, channel=None, user=None: CONTROLMAP[CREMOVEVOICECHANNEL](channel.guild.get_channel(channel.category_id), VoiceChannel),
              CREMOVEMESSAGE: lambda Text=None, TextChannel=None, channel=None, user=None: CONTROLMAP[CREMOVEMESSAGE](channel, TextChannel),
              CCLOSECLUB: lambda channel=None, user=None: CONTROLMAP[CCLOSECLUB](channel.guild.get_channel(channel.category_id)),
              CKICK: lambda Username=None, channel=None, user=None: CONTROLMAP[CKICK](channel.guild.get_channel(channel.category_id), Username),
              CADDMEMBER: lambda Username=None, channel=None, user=None: CONTROLMAP[CADDMEMBER](channel.guild.get_channel(channel.category_id), Username),
              CADDCONVENER: lambda Username=None, channel=None, user=None: CONTROLMAP[CADDCONVENER](channel.guild.get_channel(channel.category_id), get_member_by_discord_name(channel.guild, Username)),
              CCONVENERHELP: lambda channel=None, user=None: CONTROLMAP[CCONVENERHELP](channel),
              CDESCRIBE: lambda channel=None, Text=None, user=None: CONTROLMAP[CDESCRIBE](channel, Text),
              CPIN: lambda channel=None, TextChannel=None, Text=None, user=None: CONTROLMAP[CPIN](channel, TextChannel, Text),
              CUNPIN: lambda channel=None, TextChannel=None, Text=None, user=None: CONTROLMAP[CUNPIN](channel, TextChannel, Text),
              CSCHEDULEMESSAGE: lambda channel=None, Date=None, Time=None, Text=None, TextChannel=None, user=None: CONTROLMAP[CSCHEDULEMESSAGE](channel, Date, Time, Text, TextChannel),
              CUNSCHEDULEMESSAGE: lambda channel=None, Text=None, user=None: CONTROLMAP[CUNSCHEDULEMESSAGE](channel, Text),
              CWEEKLYREMINDER: lambda channel=None, Weekday=None, Time=None, Text=None, TextChannel=None, user=None: CONTROLMAP[CWEEKLYREMINDER](channel, Weekday, Time, Text, TextChannel),
              CUNSCHEDULEWEEKLYREMINDER: lambda channel=None, Text=None, user=None: CONTROLMAP[CUNSCHEDULEWEEKLYREMINDER](channel, Text),
              CSETUPEMAIL: lambda channel=None, user=None, Username=None, EmailAddress=None, EmailConfirm=None, TextChannel=None: CONTROLMAP[CSETUPEMAIL](channel, TextChannel, Username, EmailAddress, EmailConfirm),
              CREQUESTBOT: lambda channel=None, Text=None, user=None: CONTROLMAP[CREQUESTBOT](),
              GDESCRIBECLUB: lambda channel=None, ClubName=None, user=None: CONTROLMAP[GDESCRIBECLUB](channel, ClubName),
              GJOINCLUB: lambda channel=None, ClubName=None, user=None: CONTROLMAP[GJOINCLUB](ClubSuite.get_club(ClubSuite.get_all_clubs(channel.guild), ClubName), user),
              GLEAVECLUB: lambda channel=None, ClubName=None, user=None: CONTROLMAP[GLEAVECLUB](ClubSuite.get_club(ClubSuite.get_all_clubs(channel.guild), ClubName), user),
              GCLUBLIST: lambda channel=None, user=None: CONTROLMAP[GCLUBLIST](channel, GCLUBLIST),
              GMAKECLUB: lambda channel=None, user=None, Text=None: CONTROLMAP[GMAKECLUB](channel.guild, user, Text),
              GHELP: lambda channel=None, user=None: CONTROLMAP[GHELP](channel),
              UAUTHENTICATE: lambda channel=None, user=None, Text=None: CONTROLMAP[UAUTHENTICATE](channel, user, Text),
              USENDAUTHEMAIL: lambda channel=None, user=None, EmailAddress=None, EmailConfirm=None: CONTROLMAP[USENDAUTHEMAIL](channel, user, EmailAddress, EmailConfirm),
              AADDPUBCHAN: lambda channel=None, user=None, Text=None: CONTROLMAP[AADDPUBCHAN](channel.guild, user, Text),
              AADDRESTRICTCHAN: lambda channel=None, user=None, Text=None: CONTROLMAP[AADDRESTRICTCHAN](channel.guild, user, Text),
              ASETSENDERNAMEAS: lambda channel=None, user=None, Text=None: CONTROLMAP[ASETSENDERNAMEAS](channel.guild, Text),
              ASETAUTHROLE0AS: lambda channel=None, user=None, Text=None: CONTROLMAP[ASETAUTHROLE0AS](channel.guild, Text),
              ASETAUTHROLE1AS: lambda channel=None, user=None, Text=None: CONTROLMAP[ASETAUTHROLE1AS](channel.guild, Text),
              ASETAUTHCHANNAMEAS: lambda channel=None, user=None, Text=None: CONTROLMAP[ASETAUTHCHANNAMEAS](channel.guild, Text),
              ASETDESTDOMAINAS: lambda channel=None, user=None, Text=None: CONTROLMAP[ASETDESTDOMAINAS](channel.guild, Text),
              ATOGGLEAUTH: lambda channel=None, user=None: CONTROLMAP[ATOGGLEAUTH](channel.guild),
              ASETWELCOMEMESSAGEAS: lambda channel=None, user=None, Text=None: CONTROLMAP[ASETWELCOMEMESSAGEAS](channel.guild, Text),
              AUPDATE: lambda channel=None, user=None: CONTROLMAP[AUPDATE](channel.guild),
    }

TAGS={TEXT:'[Text]',
      TEXTCHANNEL:'[TextChannel]', # Always assumed you're in a category...  Will make another one for CHANNELFROMCATEGORY vs. general CHANNEL if needed.
      VOICECHANNEL:'[VoiceChannel]',
      USERNAME:'[Username]',
      EMAILADDRESS:'[EmailAddress]',
      EMAILCONFIRMATION:"[EmailConfirm]",
      TIME:'[Time]',
      WEEKDAY:'[Weekday]',
      DATE:'[Date]',
      CLUBNAME: '[ClubName]',
      PROMPTERSETUP: 'Setting_up'
      }

# These are the strings that are sent out to a user when they call a function.
# These are not strings that prompt users for specific pieces of information.
#   Those are something else. Start with standard form and then move on from there.
TAGSTRINGS= {CADDTEXTCHANNEL:TAGS[TEXT],
             CREMOVETEXTCHANNEL:TAGS[TEXTCHANNEL],
             CADDVOICECHANNEL:TAGS[TEXT],
             CREMOVEVOICECHANNEL:TAGS[VOICECHANNEL],
             CCLOSECLUB:'',
             CADDCONVENER:TAGS[USERNAME],
             CADDMEMBER:TAGS[USERNAME],
             CKICK:TAGS[USERNAME],
             CCONVENERHELP:'',
             CDESCRIBE:TAGS[TEXT],
             CPIN:TAGS[TEXT] + ' ' + TAGS[TEXTCHANNEL],
             CUNPIN:TAGS[TEXT] + ' ' + TAGS[TEXTCHANNEL],
             CREMOVEMESSAGE: TAGS[TEXT] + ' ' + TAGS[TEXTCHANNEL],
             CSCHEDULEMESSAGE: TAGS[TEXT] + ' ' + TAGS[TEXTCHANNEL] + ' ' + TAGS[TIME] + ' ' + TAGS[DATE],
             CUNSCHEDULEMESSAGE: TAGS[TEXT],
             CWEEKLYREMINDER: TAGS[TEXT] + ' ' + TAGS[TEXTCHANNEL] + ' ' + TAGS[TIME] + ' ' + TAGS[WEEKDAY],
             CUNSCHEDULEWEEKLYREMINDER: TAGS[TEXT],
             CSETUPEMAIL: TAGS[USERNAME] + ' ' + TAGS[EMAILADDRESS] + ' ' + TAGS[EMAILCONFIRMATION] + ' ' + TAGS[TEXTCHANNEL],
             CREQUESTBOT: TAGS[TEXT],
             GDESCRIBECLUB: TAGS[CLUBNAME],
             GJOINCLUB: TAGS[CLUBNAME],
             GLEAVECLUB: TAGS[CLUBNAME],
             GCLUBLIST: '',
             GMAKECLUB: TAGS[TEXT],
             GHELP: '',
             UAUTHENTICATE: TAGS[TEXT],
             USENDAUTHEMAIL: TAGS[EMAILADDRESS] + ' ' + TAGS[EMAILCONFIRMATION],
             
             AADDPUBCHAN: TAGS[TEXT],
             AADDRESTRICTCHAN: TAGS[TEXT],
             ASETSENDERNAMEAS: TAGS[TEXT],
             ASETAUTHCHANNAMEAS: TAGS[TEXT],
             ASETDESTDOMAINAS: TAGS[TEXT],
             ATOGGLEAUTH: '',
             ASETWELCOMEMESSAGEAS: TAGS[TEXT],
             ASETAUTHROLE0AS: TAGS[TEXT],
             ASETAUTHROLE1AS: TAGS[TEXT],
             AUPDATE: ''
             }

#-> Here are where the prompts for getting info for specific tags are. * prompts denote general prompts. 
#   Numbered prompts denote func enum specific prompts.
TAGPROMPTS= {GENERAL:{TAGS[TEXT]:'Message your text as this channel\'s next message! Then hit the done button!',
                      TAGS[TEXTCHANNEL]:"What channel?",
                      TAGS[VOICECHANNEL]:"What channel,?",
                      TAGS[USERNAME]:"List that member#id",
                      TAGS[EMAILADDRESS]:"What email?",
                      TAGS[EMAILCONFIRMATION]: "Please confirm your email.",
                      TAGS[TIME]:"What time?",
                      TAGS[WEEKDAY]:"What weekday?",
                      TAGS[DATE]:"What day of the year?",
                      TAGS[CLUBNAME]:"Which Club?"
                      },
             UAUTHENTICATE: {TAGS[TEXT]: "Send that auth code (everything between the quotes!) as this channel's next message. Then hit the done button!"},
             USENDAUTHEMAIL: {TAGS[EMAILADDRESS]: "Send your school email as this channe's next message. Then hit the done button!",
                              TAGS[EMAILCONFIRMATION]: "Please confirm your school email"},
    }

TEXTGENERATORS = {GCLUBLIST:ClubSuite.ui_clublist_w_descriptions}
TEXTPROMPTS = {GCLUBLIST: "Scroll back and forth to see what clubs are in our server!"}

GENERAL_OPTION_EMOJIS = {0: '\U0001F925',
                         1: '\U0001F600',
                         2: '\U0001F601',
                         3: '\U0001F929',
                         4:'\U0001F602',
                         5: '\U0001F606',
                         6: '\U0000263A',
                         7: '\U0001F917',
                         8: '\U0001F924',
                         9: '\U0001F931',
                         10: '\U0001F926',
                         11: '\U0001F927',
                         12: '\U0001F928',
                         13: '\U0001F932',
                         14: '\U0001F92A',
                         15: '\U0001F92B',
                         16: '\U0001F92C',
                         17: '\U0001F92D',
                         18: '\U0001F92E',
                         19: '\U0001F92F',
                         20: '\U0001F930',
                         
                         21: '\U0001F435',
                         22: '\U0001F412',
                         23: '\U0001F98D',
                         24: '\U0001F9A7',
                         25: '\U0001F436',
                         26: '\U0001F415',
                         27: '\U0001F9AE',
                         28: '\U0001F429',
                         29: '\U0001F415',
                         30: '\U0001F43A',
                         31: '\U0001F98A',
                         32: '\U0001F99D'}
