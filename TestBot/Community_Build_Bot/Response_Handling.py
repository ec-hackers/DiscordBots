# -*- coding: utf-8 -*-
"""
Created on Thu Oct  8 19:00:45 2020

@author: josep
"""

RESPONSE_CODES = {1: "SUCCEEDED",
                  2: "SUCCESSFULLY AUTHENTICATED",
                  3: "SUCCESSFULLY SENT EMAIL TO SUCH AND SUCH", 
                  4: "SUCCESSFULLY CLOSED SPACE",
                  5: "REQUEST BEING PROCESSED AS WE SPEAK",
                  6: "WE WILL WORK ON THAT BOT FOR YOU",
                  7: "SUCCESSFULLY ADDED A VOICE CHANNEL",
                  8: "SUCCESSFULLY SET ENCRYPTED INFORMATION",
                  0:'NOT HANDLED HERE EXCEPTION', 
                  -1: "NO USER USERNAME", 
                  -2: "NO SUCH TEXTCHANNEL",
                  -3: "NO SUCH VOICE CHANNEL",
                  -4: "ABSTRACT CATEGORY ALREADY EXISTS",
                  -5: "CLUB ALREADY EXISTS",
                  -6: "RESTRICT CHAN ALREADY EXISTS",
                  -7: "OPEN CHAN ALREADY EXISTS",
                  -8: "SPECIAL CHAR ERROR",
                  -9: "NO CLUB DESCRIPTION",
                  -10: "CLUB NOT FOUND ERROR",
                  -11: "UNABLE TO UNSCHEDULE TASK ERROR",
                  -12: "MEETING UNCANCELABLE ERROR",
                  -13: "3 ARGS REQUIRED FOR COMMAND ERROR",
                  -14: "Cant find message in channel error",
                  -15: "NOT A TEXT CHANNEL IN CATEGORY",
                  -16: "LIST NOT CLOSED ERROR",
                  -17: "NOT A WEEKDAY ERROR",
                  -18: "INVALID TIME FORMAT ERROR",
                  -19: "EMAILSETUP NEEDS 3 PARTS ERROR",
                  -21: "USER NOT FOUND ERROR",
                  -22: 'ADMIN CHAN ALREADY EXISTS',
                  -23: 'NOT IN VAR ERROR',
                  -24: "Too many args error",
                  -25: "YOU DONT HAVE PERMISSION TO ACCESS",
                  }

async def handle_response_codes(message, code):
    message_parts = message.content.split(' ')
    if code == 1:
        pass#await message.channel.send('Succeeded...', delete_after = 3)
    elif code == 2:
        await message.channel.send('Successfully Authenticated... Moving to full discord.', delete_after = 3)
    elif code == 3:
        await message.channel.send("Sent email to " + message.content[1:], delete_after = 3)
    elif code == 4:
        pass
    elif code == 5:
        await message.channel.send('We heard your request. Our moderators will respond to your request soon :)', delete_after = 3)
    elif code == 6:
        await message.channel.send('We will get back to you soon with questions and information about your bot!', delete_after = 3)
    elif code == 7:
        pass
    elif code == 8:
        await message.channel.send('Successfully saved encrypted information. Deleting your unencrypted message...')
        await message.delete()
    elif code == 0:
        await message.channel.send("No command called: " + message.content.split(' ')[0] + ". Try again?", delete_after = 3)
    elif code == -1:
        await message.channel.send("No User called" + message.content.split(' ')[1] + " in the server. Try their name again or have them join? Example username: ejnislam18#4757", delete_after = 3)
    elif code == -2:
        await message.channel.send("No Such Text Channel: " + message_parts[1], delete_after = 3)
    elif code == -3:
        await message.channel.send("No Such Voice Channel: " + message_parts[1], delete_after = 3)
    elif code == -4:
        await message.channel.send("ABSTRACT CHANNEL ALREADY EXISTS: " + message_parts[1], delete_after = 3)
    elif code == -5:
        await message.channel.send("Sorry... A club by the name " + message_parts[1] + " already exists... Try joining that one? Get help with !help", delete_after = 3)
    elif code == -6:
        await message.channel.send("Sorry... A RestrictChan by the name " + message_parts[1] + " already exists... Try joining that one? Get help with !help", delete_after = 3)
    elif code == -7:
        await message.channel.send("Sorry... A Public Channel by the name " + message_parts[1] + " already exists... Try joining that one? Get help with !help", delete_after = 3)    
    elif code == -9:
        await message.channel.send('Aww... ' + '-'.join(message_parts[1:]) + ' hasn\'t set a description yet. Check back later!', delete_after = 3)
    elif code == -10:
        await message.channel.send('Sorry... I couldn\'t find a club called ' + '-'.join(message_parts[1:]) + '. Try again and check your spelling?', delete_after = 3)
    elif code == -11:
        await message.channel.send('Sorry.... I couldn\'t unschedule ' + message.content[len('!unschedule '):] + '. Check your copy-pasting and try again? Copy paste the w-h-o-l-e thing!', delete_after = 3)
    elif code == -12:
        await message.channel.send('Sorry.... I couldn\'t cancel your meeting... ' + message.content[len('!cancelmeetingtime '):] +  '. Check your copy-pasting and try again? Copy paste the w-h-o-l-e thing!', delete_after = 3)
    elif code == -13:
        await message.channel.send('!removemessage requires 3 things! Here\'s an example: !removemessage MyClubTextChannel text from message i want to remove', delete_after = 10)
    elif code == -14:
        await message.channel.send("I couldn't find " + ' '.join(message_parts[2:]), 'in' + message_parts[1], delete_after = 3)
    elif code == -15:
        await message.channel.send("Sorry... I couldn't find one of your text channels in your category... Try again?", delete_after = 3)
    elif code == -16:
        await message.channel.send("Hey! Watch your typing! You need a closing bracket ']' if you're going to finish that list correctly!", delete_after = 3)
    elif code == -17:
        await message.channel.send("Hey!", message_parts[1], "isn't a weekday!", delete_after = 3)
    elif code == -18:
        await message.channel.send('Hey! The time part here needs a colon. hr:min in military time. example: 14:02', delete_after = 3)
    elif code == -19:
        await message.channel.send('Hey! !emailsetup takes 2 parameters! as !emailsetup discordname#id:destination_email channel_to_be_active_in !' , delete_after = 3)
    elif code == -21:
        await message.channel.send("Hey! We couldn't find that user in the discord server at all! Check your spelling and make sure you got their #4numid at the end of their name!", delete_after = 3)
    elif code == -23:
        await message.channel.send('Hey! That variable isn\'t in var! Try again!', delete_after = 3)
    elif code == -24:
        await message.channel.send("Hey! That is too many arguments for this command!", delete_after = 3)
    elif code == -25: 
        await message.channel.send("You don't have permission to execute: " + message_parts[0], delete_after = 3)
    elif type(code) == tuple:
        if len(code) == 3 and code[0]==-8:
            await message.channel.send('Please no special characters in clubnames (' + code[2] + "). You put: " + code[1], delete_after = 3)
    else:
        await message.channel.send("You have encountered a mystery error! Reach out to Joseph Islam and he will love to help you get what you're trying to do done. ASAP.", delete_after = 10)
    return 1