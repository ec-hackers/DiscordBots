# -*- coding: utf-8 -*-
"""
Created on Thu Oct  8 10:08:06 2020

@author: josep
"""
import discord

from handles import general_user_role_names, server_vars, key
from AbstractChannel import AbstractChannel, AbstractChanSuite
from PublicChannel import PublicChan, PubChanSuite
from restrictedchannel import RestrictChan
from cryptography.fernet import Fernet


async def do_update(guild):
    await AdminChanSuite.refresh_active_vars(guild)
    await AdminChanSuite.ui_set_version(guild)
    return 1

async def assert_auth(guild):
    auth_channel_name = await AdminChanSuite.get_auth_channel_name(guild)
    auth_role_name = await AdminChanSuite.get_auth_channel_name(guild) # Needs a seperate server var variable.
    auth_channel = None
    auth_role = None
    for channel in guild.text_channels:
        if channel.name == auth_channel_name:
            auth_channel = channel
    if auth_channel == None:
        auth_channel = await guild.create_text_channel(auth_channel_name)
        await (await auth_channel.send(await AdminChanSuite.get_auth_chan_message(guild))).pin()
        await (await auth_channel.fetch_message(channel.last_message_id)).delete()
        
    for role in guild.roles:
        if role.name == auth_role_name:
            auth_role = role
    if auth_role == None:
        auth_role = await guild.create_role(name=auth_role_name)
    await auth_channel.set_permissions(guild.default_role, read_messages = False)
    await auth_channel.set_permissions(auth_role, read_messages=True, send_messages=True)
    return (auth_channel, auth_role)
    
    

class AdminChannel(AbstractChannel):
    Convenor_cli_permissions = {'read_messages':True,
                                'send_message':True,
                                'view_channel':True,
                                'connect':True,
                                'speak':True}
    Member_permissions = {'read_messages':True,
                          'send_messages':True,
                          'connect':True,
                          'speak':True}
    
    default_guild_role_perms = {'read_messages':False,
                                'view_channel':False,
                                'send_messages':False,
                                'connect':False,
                                'speak': False}
    convenor_color = discord.Colour.red()
    Member_abstract_permissions = None
    channel_type = 'A'
    convener_role_name = 'Admin'
    member_role_name = 'none'
    member_color = discord.Colour.red()
    category_name = "Server"
    convenor_color = discord.Colour.red()

    def __init__(self, guild, moderator): # Should auto initialize the var section in this constructor.
        # Make a cli where commands from club conveners here can shape club below.
        # Have list of all voice and text channels owned by the club.
        # Can only influence voice/text channels owned by this club object.
        # Clubs are the only things that can act on the server. Conveners can act
        # on club objects.

        #Auto give at least one person convener access to the club.
        # Needs functionality to give other ECStudents convener roles.
        AbstractChannel.__init__(self, guild, moderator, AdminChannel.category_name,
                                 AdminChannel.channel_type,
                                 AdminChannel.convener_role_name,
                                 AdminChannel.member_role_name,
                                 AdminChannel.convenor_color,
                                 AdminChannel.member_color,
                                 AdminChannel.default_guild_role_perms,
                                 AdminChannel.Member_permissions,
                                 AdminChannel.Convenor_cli_permissions)
                
        
    async def _init(self):
        response = await AbstractChannel._init(self, [], suite=AdminChanSuite) # Add a var channel too and populate it with necessary fields for server functioning.
        if response != 1:
            return response
        # get the admin category and add a var to it
        cat = AdminChanSuite.get_admincat(self._guild)
        var_chan = await AbstractChanSuite.add_text_channel(cat, 'var')
        var_chan = await AbstractChanSuite.get_text_channel(cat, 'var')
        var_chan.topic = 'DO NOT EDIT MANUALLY! This is the section that stores information relevant for your server! Use !adminvarsetup to begin to get this set up!'
        for var, initialized_to in server_vars.items():
            await var_chan.send(var + initialized_to)
        for role in self._chan_roles:
            await var_chan.set_permissions(role, send_messages=False)

class AdminChanSuite(AbstractChanSuite):
    async def refresh_active_vars(guild):
        cat = AdminChanSuite.get_admincat(guild)
        if not AdminChanSuite.get_admincat(guild):
            achan = AdminChannel()
            await achan._init()
            cat = AdminChanSuite.get_admincat(guild)
        if not await AbstractChanSuite.get_text_channel(cat, 'var'):
            var_chan = await AbstractChanSuite.add_text_channel(cat, 'var')
            var_chan = await AbstractChanSuite.get_text_channel(cat, 'var')
            for role in guild.roles:
                await var_chan.set_permissions(role, send_messages=False)
        
        await AdminChanSuite._delete_depreciated_vars(guild)
        await AdminChanSuite._add_vars(guild)
    
    async def _delete_depreciated_vars(guild):
        server_var_keys = set(server_vars.keys())
        cat = AdminChanSuite.get_admincat(guild)
        var = await AbstractChanSuite.get_text_channel(cat, 'var')
        async for message in var.history(limit=200):
            server_var_parts = message.content.split('=')
            if server_var_parts[0] + "=" not in server_var_keys:
                await message.delete()
    # inefficient
    async def _add_vars(guild):
        cat = AdminChanSuite.get_admincat(guild)
        var = await AbstractChanSuite.get_text_channel(cat, 'var')
        for server_var, default_value in server_vars.items():
            if await AdminChanSuite.get_var_message(guild, server_var) == -23:
                await var.send(server_var + default_value)
        
    async def get_var_message(guild, var_name):
        cat = AdminChanSuite.get_admincat(guild)
        var = await AbstractChanSuite.get_text_channel(cat, 'var')
        async for message in var.history(limit=200):
            if message.content.startswith(var_name):
                return message
        return -23
    async def get_auth_enabled(guild):
        return AdminChanSuite.get_var_val_from_message(await AdminChanSuite.get_var_message(guild, 'auth_channel_enabled=')) == "True"
    async def get_auth_chan_message(guild):
        return AdminChanSuite.get_var_val_from_message(await AdminChanSuite.get_var_message(guild, 'auth_channel_welcome_text='))
    async def get_sender_name(guild):
        return AdminChanSuite.get_var_val_from_message(await AdminChanSuite.get_var_message(guild, 'sender_name='))
    async def get_authrolename0(guild):
        return AdminChanSuite.get_var_val_from_message(await AdminChanSuite.get_var_message(guild, 'auth_role_name0='))
    async def get_authrolename1(guild):
        return AdminChanSuite.get_var_val_from_message(await AdminChanSuite.get_var_message(guild, 'auth_role_name1='))
    async def get_auth_channel_name(guild):
        return AdminChanSuite.get_var_val_from_message(await AdminChanSuite.get_var_message(guild, 'auth_channel_name='))
    async def get_email_verification_destination_domain(guild):
        return AdminChanSuite.get_var_val_from_message(await AdminChanSuite.get_var_message(guild, 'email_verification_destination_domain='))
    async def get_general_user_role_names(guild):
        return [await AdminChanSuite.get_authrolename0(guild), await AdminChanSuite.get_authrolename1(guild)]
    
    def _assert_non_duplicate(guild, channel_name):
        if AbstractChanSuite._assert_non_duplicate(guild, AdminChannel.channel_type, channel_name) == -4:
            return -22
        return 1
    def get_all_adminchans(guild):
        return AbstractChanSuite.get_all_chans(guild, AdminChannel.channel_type)
    # Gets the Admin Category. returns false if it doesn't exist.
    def get_admincat(guild):
        return AbstractChanSuite.get_chan(AdminChanSuite.get_all_adminchans(guild),'A-Server')
    
    # gets the admin cli out of the admin category.
    # false if it doesn't exist.
    def get_cli(category):
        if category:
            return AbstractChanSuite.get_cli(category, AdminChannel.convener_role_name)
        
    def is_cli(guild, channel):
        return AbstractChanSuite.is_cli(AdminChanSuite.get_all_adminchans(guild), 
                                        channel, AdminChannel.convener_role_name)
    async def add_admin(channel, member):
        await AbstractChanSuite.add_CLIUser(channel, member, AdminChannel.convener_role_name)
    async def remove_as_moderator(chan, member):
        await member.remove_roles(AdminChanSuite.get_role(chan, chan.name + ' ' + AdminChannel.convener_role_name))
        
    def get_admin_commands():
        return ['Unimplemented']
    def get_restrictchan_command_examples():
        return ['Unimplemented']
    def get_help_message(channel):
        return 'Unimplemented'
    
    async def set_var(message, new_value):
        # Encrypt the unencrypted string and post it in the message's associated content.
        
        split_at = message.content.index('=')
        var = message.content[:split_at + 1]
        await message.edit(content=var + new_value)
        return 1
    def get_var_val_from_message(message):
        split_at = message.content.index('=')
        var = message.content[split_at + 1:]
        return var
    def encrypt(unencrypted_string):
        unencrypted_string = unencrypted_string.encode()
        f = Fernet(key)
        encrypted_string = f.encrypt(unencrypted_string).decode()
        return encrypted_string
    def decrypt(encrypted_string):
        f = Fernet(key)
        encrypted_string=encrypted_string.encode()
        return f.decrypt(encrypted_string).decode()
    
    async def ui_add_restrictchan(guild, author, name):
        new_restrictChan = RestrictChan(guild, author, name)
        return await new_restrictChan._init()
    
    async def ui_add_pubchan(guild, author, name):
        new_pubchan = PublicChan(guild, author, name)
        return await new_pubchan._init(general_user_role_names)
        
    async def ui_set_sender_name_as(guild, sender_name):
        var_message = await AdminChanSuite.get_var_message(guild, 'sender_name=')
        if var_message == -23:
            return -23
        return await AdminChanSuite.set_var(var_message, sender_name)
    
    async def ui_set_authrole0_as(guild, authrole0name):
        var_message = await AdminChanSuite.get_var_message(guild, 'auth_role_name0=')
        if var_message == -23:
            return -23
        return await AdminChanSuite.set_var(var_message, authrole0name)
    
    async def ui_set_authrole1_as(guild, authrole1name):
        var_message = await AdminChanSuite.get_var_message(guild, 'auth_role_name1=')
        if var_message == -23:
            return -23
        return await AdminChanSuite.set_var(var_message, authrole1name)
    
    async def ui_set_auth_chan_name_as(guild, auth_chan_name):
        var_message = await AdminChanSuite.get_var_message(guild, 'auth_channel_name=')
        if var_message == -23:
            return -23
        return await AdminChanSuite.set_var(var_message, auth_chan_name)
    
    async def ui_set_domain_dest_as(guild, dest):
        var_message = await AdminChanSuite.get_var_message(guild, 'email_verification_destination_domain=')
        if var_message == -23:
            return -23
        return await AdminChanSuite.set_var(var_message, dest)
    
    async def ui_toggle_auth(guild):
        var_message = await AdminChanSuite.get_var_message(guild, 'auth_channel_enabled=')
        val_bool = await AdminChanSuite.get_auth_enabled(guild)
        if not val_bool:
            await assert_auth(guild)
        auth_name = await AdminChanSuite.get_auth_channel_name(guild)
        new_text = await AdminChanSuite.get_auth_chan_message(guild)
        for channel in guild.text_channels:
            if channel.name == auth_name:
                for msg in await channel.pins(): # The only pinned message that can exist in auth is the one the bot sent lol
                    await msg.edit(content = new_text)
                    break
        return await AdminChanSuite.set_var(var_message, str(not val_bool))
    
    async def ui_set_welcome_message_as(guild, new_text):
        var_message = await AdminChanSuite.get_var_message(guild, 'auth_channel_welcome_text=')
        if var_message == -23:
            return -23
        await AdminChanSuite.set_var(var_message, new_text)
        auth_name = await AdminChanSuite.get_auth_channel_name(guild)
        for channel in guild.text_channels:
            if channel.name == auth_name:
                for msg in await channel.pins(): # The only pinned message that can exist in auth is the one the bot sent lol
                    await msg.edit(content = new_text)
                    break
        return 1
    async def ui_update(guild):
        return await do_update(guild)
    
    async def ui_set_version(guild):
        var_message = await AdminChanSuite.get_var_message(guild, 'version=')
        if var_message == -23:
            return -23
        return await AdminChanSuite.set_var(var_message, server_vars['version='])
    
    async def process_cli_commands(message):
        # for case sensitive commands
        raw_text = message.content
        raw_text_parts = raw_text.split(' ')
        
        # for not case sensitive commands
        lower_text = raw_text.lower()
        lower_text_parts = lower_text.split(' ')
        # Admin category andcli for executing commands down the line.
        admin_category = AdminChanSuite.get_admincat(message.guild)
        admin_cli = AdminChanSuite.get_cli(admin_category)
        print(admin_category, admin_cli)
        if not admin_category or not admin_cli:
            return False
        
        '''
        Case unsensitive commands go first
        '''
        if lower_text.startswith('!addpubchan'):
            pubName = lower_text_parts[1]
            
            # Making a new channel should ALWAYS AUTOMATICALLY check for duplicates.
            return await AdminChanSuite.ui_add_pubchan(message.guild, message.author, pubName)
             # when you come back, finish the auto check for dupes functions.
        elif lower_text.startswith('!addrestrictchan'):
            resName = lower_text_parts[1]
            return await AdminChanSuite.ui_add_pubchan(message.guild, message.author, resName)
        elif lower_text.startswith('!setsendernameas '):
            sender_name = message.content[len('!setsendernameas '):]
            return await AdminChanSuite.ui_set_sender_name_as(message.guild, sender_name)
        
        elif lower_text.startswith('#!setemailpassas '):
            if len(raw_text_parts) != 2:
                return -24
            encrypted_pass = AdminChanSuite.encrypt(raw_text_parts[1])
            var_message = await AdminChanSuite.get_var_message(message.guild, 'associated_email_password_encrypted=')
            if var_message == -23:
                return -23
            if await AdminChanSuite.set_var(var_message, encrypted_pass) == 1:
                return 8
        elif lower_text.startswith('!setauthrole0as'):
            authrole0name = message.content[len('!setauthrole0as '):]
            return await AdminChanSuite.ui_set_authrole0_as(message.guild, authrole0name)
        elif lower_text.startswith('!setauthrole1as'):
            authrole1name = message.content[len('!setauthrole1as '):]
            return await AdminChanSuite.ui_set_authrole1_as(message.guild, authrole1name)
        elif lower_text.startswith('!setauthchannameas'):
            if len(raw_text_parts) != 2:
                return -24
            return await AdminChanSuite.ui_set_auth_chan_name_as(message.guild, raw_text_parts[1])
        elif lower_text.startswith('!setdestdomainas'):
            if len(raw_text_parts) != 2:
                return -24
            return await AdminChanSuite.ui_set_domain_dest_as(message.guild, raw_text_parts[1])
        elif lower_text.startswith('!toggle_auth'):
            return await AdminChanSuite.ui_toggle_auth(message.guild)
        elif lower_text.startswith('!setwelcomemessageas '):
            new_text = message.content[len('!setwelcomemessageas '):]
            return await AdminChanSuite.ui_set_welcome_message_as(message.guild, new_text)
        elif lower_text.startswith('!update'):
            return await AdminChanSuite.ui_update(message.guild)
        
        return 0
     
        '''
        Case sensitive commands go second
        '''
        
    async def process_user_commands(message):
        return False # There are never user commands in this channel because there are no users allowed.
    async def process_commands(message):
        return await AdminChanSuite.process_cli_commands(message) or await AdminChanSuite.process_user_commands(message)
    async def process_tasks(clis):
        pass
