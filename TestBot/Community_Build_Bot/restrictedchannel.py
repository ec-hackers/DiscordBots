# -*- coding: utf-8 -*-
"""
Created on Sat Sep  5 14:34:41 2020

@author: josep
"""

# Restricted Channel:
#   Channels where people must get moderator approval to add new text 
#   and voice channels

import discord
from AbstractChannel import AbstractChanSuite, AbstractChannel
# Public Channels are areas where people can make text and voice channels
# Whenever they want to

class RestrictChan: # Creates a restricted category channel with CLUB MEMBER role read and write permissions for club members and CLI access for club convenors
    # Is the same as a public channel, except that 
    Convenor_cli_permissions = {'read_messages':True,
                                'send_message':True}
    Member_permissions = {'read_messages':True,
                          'send_messages':True,
                          'connect':True,
                          'speak':True}
    default_guild_role_perms = {'read_messages':True,
                                'send_messages':True,
                                'connect':True,
                                'speak': True}
    convenor_color = discord.Colour.red()
    member_color = discord.Color.greyple()
    Member_abstract_permissions = None
    channel_type = 'r'
    convener_role_name = 'Moderator_'
    member_role_name = 'none'
    convenor_color = discord.Colour.red()
    def __init__(self, guild, moderator, channelName):
        # Make a cli where commands from club conveners here can shape club below.
        # Have list of all voice and text channels owned by the club.
        # Can only influence voice/text channels owned by this club object.
        # Clubs are the only things that can act on the server. Conveners can act
        # on club objects.

        #Auto give at least one person convener access to the club.
        # Needs functionality to give other ECStudents convener roles.
        AbstractChannel.__init__(self, guild, moderator, channelName,
                                 RestrictChan.channel_type,
                                 RestrictChan.convener_role_name,
                                 RestrictChan.member_role_name,
                                 RestrictChan.convenor_color,
                                 RestrictChan.member_color,
                                 RestrictChan.default_guild_role_perms,
                                 RestrictChan.Member_permissions,
                                 RestrictChan.Convenor_cli_permissions)
                
        
    async def _init(self, general_user_roles):
        return await AbstractChannel._init(self, general_user_roles,
                              suite=RestrictChanSuite)
        
class RestrictChanSuite(AbstractChanSuite):
    '''
    Checks to make a channel does not already exist.
    '''
    def _assert_non_duplicate(guild, channel_name):
        if AbstractChanSuite._assert_non_duplicate(guild, RestrictChan.channel_type, channel_name) == -4:
            return -6
        return 1
        
    def get_all_restrictchans(guild): # returns list of club categories
        return AbstractChanSuite.get_all_chans(guild, RestrictChan.channel_type)
    
    '''
    Resolve club category from club category name
    '''
    def get_restrictchan(chans, name):
        return AbstractChanSuite.get_chan(chans, name)
    
    def get_restrictchan_roles(chan):
        return AbstractChanSuite.get_chan_roles(chan)
    
    def get_cli(category):
        return AbstractChanSuite.get_cli(category, RestrictChan.convener_role_name)
    
    '''
    Returns club category that matches proposed cli if there is one.
    '''
    def is_cli(categories, channel):
        return AbstractChanSuite.is_cli(categories, channel, RestrictChan.convener_role_name)
    # Add new convener to the club
    async def add_moderator(club, member):
        await AbstractChanSuite.add_CLIUser(club, member, RestrictChan.convener_role_name)
    async def remove_as_moderator(chan, member):
        await member.remove_roles(RestrictChanSuite.get_role(chan, chan.name + ' ' + RestrictChan.convener_role_name))
    async def ban_member(self, member):
        # permaban member from the pubchannel
        # do not allow join if on a list, i guess
        pass
    def unban_member(self, member):
        # unban member from the pubchannel
        # remove from the list, i guess
        pass
    
    '''
    add text channel to resolved club category channel
    '''
    
    def get_restrictchan_commands():
        return ['!addrole - notimplemented','!addmember [member]- adds a member','!kick [member] - removes a member','!addvoicechannel [name] - adds a voice channel','!addtextchannel [name] - adds a text channel','!addconvener [member] - makes [member] a convener', '!ban [member] - unimplemented', '!unban [member] - unimplemented', '!closeclub - closes your club',
                '!removevoicechannel [name] - remove voice channel', '!removetextchannel [name] - remove text channel']
    def get_restrictchan_command_examples():
        return # not implemented
    
    def get_help_message(channel):
        commands = RestrictChanSuite.get_restrictchan_commands()
        send_text = 'Moderator_ Commands in ' + channel.name + ' are:\n\t'
        for command in commands:
            send_text += command + '\n\t'
        return send_text
    async def process_cli_commands(message):
        content = message.content.lower()
        message_parts = content.split(' ')
        res_chan = AbstractChanSuite.is_cli(AbstractChanSuite.get_all_chans(message.guild, RestrictChan.channel_type), message.channel, RestrictChan.convener_role_name)
        if not res_chan:
            return False # Did not process commands
        if content.startswith('!addtextchannel'):
            await RestrictChanSuite.add_text_channel(res_chan, content[len('!addtextchannel '):])
        elif content.startswith('!removetextchannel'):
            await RestrictChanSuite.remove_text_channel(res_chan, content[len('!removetextchannel '):])
        elif content.startswith('!addvoicechannel'):
            await RestrictChanSuite.add_voice_channel(res_chan, content[len('!addvoicechannel '):])
        elif content.startswith('!removevoicechannel'):
            await RestrictChanSuite.remove_voice_channel(res_chan, content[len('!removevoicechannel '):])
        elif content.startswith('!removemessage'):
            terms = message.content.split(' ')
            terms[1] = message_parts[1]
            if len(terms) < 3:
                await message.channel.send('!removemessage requires 3 things! Here\'s an example: !removemessage MyClubTextChannel text from message i want to remove', delete_after = 10)
            channel = await RestrictChanSuite.get_text_channel(res_chan, terms[1])
            content = ' '.join(terms[2:])
            if await RestrictChanSuite.remove_text_msg_from_chan(channel, content):
                return True
            await message.channel.send('Couldn\'t find "' + content + '" in ' + terms[1], delete_after = 10)
        return True
    async def process_user_commands(message):
        # Check if a valid command and respond accordingly if so.
        # Return false if no command was processed.
        # Send a success message and return true if a command was processed successfully.
        content = message.content.lower()
        message_parts = content.split(' ')
        if len(message_parts) >= 3:
            rschan = RestrictChanSuite.get_restrictchan(RestrictChanSuite.get_all_restrictchans(message.channel.guild),RestrictChan.channel_type +'-'+ message_parts[1])
            rschan_cli = RestrictChanSuite.get_cli(rschan)
            addchanname = '-'.join(message_parts[2:])
            if rschan:
                if content.startswith('!addtextchannel'):
                    await rschan_cli.send(message.author.name + ' would like to add a text channel to this restricted group: ' + addchanname + '. Would anyone like to take care of that?')
                    return 5
                    # remove a text channel from a hangout space.
                elif content.startswith('!removetextchannel'):
                    await rschan_cli.send(message.author.name + ' would like to remove a text channel to this restricted group: ' + addchanname + '. Would anyone like to take care of that?')
                    return 5
                elif content.startswith('!addvoicechannel'):
                    await rschan_cli.send(message.author.name + ' would like to add a voice channel to this restricted group: ' + addchanname + '. Would anyone like to take care of that?')
                    return 5
                elif content.startswith('!removevoicechannel'):
                    await rschan_cli.send(message.author.name + ' would like to remove a voice channel to this restricted group: ' + addchanname + '. Would anyone like to take care of that?')
                    return 5
        return 0
    async def process_commands(message):
        return await RestrictChanSuite.process_cli_commands(message) or await RestrictChanSuite.process_user_commands(message)
    async def process_tasks(clis):
        pass